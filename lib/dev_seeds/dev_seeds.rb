module DevSeeds
  class << self
    def import
      identities = Identity.create([{
        name: "u1",
        email: "u1@users.com",
        password: "123123",
        password_confirmation: "123123"
      }, {
        name: "u2",
        email: "u2@users.com",
        password: "123123",
        password_confirmation: "123123"
      }, {
        name: "u3",
        email: "u3@users.com",
        password: "123123",
        password_confirmation: "123123"
      }])
  
      users = []
      identities.each do |identity|
        users << User.create({
          provider: "identity",
          uid: identity.id,
          display_name: identity.name,
          email: identity.email,
          chosen_locale_code: :"zh-HK",
          chosen_bible_code: "unv"
        })
      end
  
      users[0].follow!(users[1])
      users[1].follow!(users[0])
      users[1].follow!(users[2])
  
      virtual_churches = VirtualChurch.create([{
        creator: users[0],
        name: "VC1",
        description: "The church created by #{users[0].display_name}"
      }, {
        creator: users[0],
        name: "VC2",
        description: "Another church created by #{users[0].display_name}"
      }, {
        creator: users[1],
        name: "VC3",
        description: "The church created by #{users[1].display_name}"
      }])
  
      users[1].join!(virtual_churches[0])
      users[2].join!(virtual_churches[0])
      users[2].join!(virtual_churches[1])
    end
  end
end