module BibleContent
  class Converter
    def initialize(bible_code, file_path)
      @bible_code = bible_code
      @file_path = Rails.root.join("lib", "bible_content", "bibles", file_path)
      
      raise "File not exist" unless File.exist?(@file_path)
      raise "Bible code has been chosen! Please use another code!" if Verse.where(bible_code: @bible_code).count > 0
    end
    
    def process
      import_time = Benchmark.ms do
        import
      end
      
      puts "Import time (#{@bible_code} at #{@file_path}): #{import_time/1000}s"
    end
    
    def import
      file = File.new(@file_path, "r")
      while line = file.gets
        dummy_line, dummy_id, book_short_name, chapter_num, verse_num, content = *line.match(/^([0-9]+)#([^#\s]+|\d [^#\s]+)\s+#([0-9]+)#([0-9]+)#(.+)$/)
        
        Verse.create!({
          bible_code: @bible_code,
          book_code: book_short_name.downcase.gsub(/ /, ""),
          chapter: chapter_num.to_i,
          verse: verse_num.to_i,
          content: content
        })
      end
    end
  end
end