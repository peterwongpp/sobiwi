# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

if Verse.count > 0
  puts "As there are verses, we will not import again!"
else
  puts "As there is no verse, we will now import the bibles..."
  puts "importing KJV Bible..."
  BibleContent::Converter.new("kjv", "dnstrkjv").process
  puts "importing UNV Bible..."
  BibleContent::Converter.new("unv", "dnstrunv").process
  puts "Bibles imported."
end

unless Rails.env.production?
  Identity.delete_all
  identities = [
    FactoryGirl.create(:identity),
    FactoryGirl.create(:identity)
  ]

  User.delete_all
  users = identities.each_with_object([]) do |identity, array|
    array << FactoryGirl.create(:user, uid: identity.id)
  end

  Followship.delete_all
  FactoryGirl.create(:followship, {
    user_id: users[0].id,
    following_user_id: users[1].id
  })

  VirtualChurch.delete_all
  virtual_churches = [
    FactoryGirl.create(:virtual_church, creator: users[0]),
    FactoryGirl.create(:virtual_church, creator: users[1])
  ]

  Membership.delete_all
  FactoryGirl.create(:membership, {
    user: users[0],
    virtual_church: virtual_churches[1]
  })

  Note.delete_all
  notes = [
    FactoryGirl.create(:note_share, user: users[0]),
    FactoryGirl.create(:note_share, user: users[1]),
    FactoryGirl.create(:note_note, user: users[0]),
    FactoryGirl.create(:note_note, user: users[1]),
    FactoryGirl.create(:note_share, user: users[0]),
    FactoryGirl.create(:note_note, user: users[1]),
    FactoryGirl.create(:note_note, user: users[0]),
    FactoryGirl.create(:note_share, user: users[1])
  ]

  Annotation.delete_all
  FactoryGirl.create(:annotation, {
    note: notes[0], verse: Verse.where(bible_code: "kjv", book_code: "gen", chapter: 1, verse: 1).first,
    from_atom_index:  0,
    to_atom_index:    12
  })
  FactoryGirl.create(:annotation, {
    note: notes[1], verse: Verse.where(bible_code: "kjv", book_code: "gen", chapter: 1, verse: 2).first,
    from_atom_index:  6,
    to_atom_index:    (Verse.where(bible_code: "kjv", book_code: "gen", chapter: 1, verse: 2).first.atoms.count-1)
  })
  FactoryGirl.create(:annotation, {
    note: notes[1], verse: Verse.where(bible_code: "kjv", book_code: "gen", chapter: 1, verse: 3).first,
    from_atom_index:  0,
    to_atom_index:    18
  })
  FactoryGirl.create(:annotation, {
    note: notes[2], verse: Verse.where(bible_code: "kjv", book_code: "gen", chapter: 1, verse: 3).first,
    from_atom_index:  20,
    to_atom_index:    25
  })
  FactoryGirl.create(:annotation, {
    note: notes[3], verse: Verse.where(bible_code: "kjv", book_code: "gen", chapter: 1, verse: 4).first,
    from_atom_index:  3,
    to_atom_index:    18
  })
  FactoryGirl.create(:annotation, {
    note: notes[4], verse: Verse.where(bible_code: "kjv", book_code: "gen", chapter: 1, verse: 4).first,
    from_atom_index:  19,
    to_atom_index:    30
  })
  FactoryGirl.create(:annotation, {
    note: notes[5], verse: Verse.where(bible_code: "kjv", book_code: "gen", chapter: 1, verse: 4).first,
    from_atom_index:  31,
    to_atom_index:    50
  })
  FactoryGirl.create(:annotation, {
    note: notes[6], verse: Verse.where(bible_code: "kjv", book_code: "gen", chapter: 1, verse: 5).first,
    from_atom_index:  0,
    to_atom_index:    12
  })
  FactoryGirl.create(:annotation, {
    note: notes[7], verse: Verse.where(bible_code: "kjv", book_code: "gen", chapter: 1, verse: 5).first,
    from_atom_index:  14,
    to_atom_index:    22
  })
end