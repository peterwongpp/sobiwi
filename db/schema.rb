# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120407074045) do

  create_table "annotations", :force => true do |t|
    t.integer  "note_id"
    t.integer  "verse_id"
    t.integer  "from_atom_index"
    t.integer  "to_atom_index"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "annotations", ["note_id"], :name => "index_annotations_on_note_id"
  add_index "annotations", ["verse_id"], :name => "index_annotations_on_verse_id"

  create_table "followships", :force => true do |t|
    t.integer  "user_id"
    t.integer  "following_user_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "followships", ["following_user_id"], :name => "index_followships_on_following_user_id"
  add_index "followships", ["user_id"], :name => "index_followships_on_user_id"

  create_table "identities", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "forget_password_token"
    t.datetime "forget_password_token_created_at"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  add_index "identities", ["forget_password_token"], :name => "index_identities_on_forget_password_token"

  create_table "memberships", :force => true do |t|
    t.integer  "user_id"
    t.integer  "virtual_church_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "memberships", ["user_id"], :name => "index_memberships_on_user_id"
  add_index "memberships", ["virtual_church_id"], :name => "index_memberships_on_virtual_church_id"

  create_table "message_thread_users", :force => true do |t|
    t.integer  "message_thread_id"
    t.integer  "user_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "message_thread_users", ["message_thread_id"], :name => "index_message_thread_users_on_message_thread_id"
  add_index "message_thread_users", ["user_id"], :name => "index_message_thread_users_on_user_id"

  create_table "message_threads", :force => true do |t|
    t.string   "uniq_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "message_threads", ["uniq_id"], :name => "index_message_threads_on_uniq_id"

  create_table "messages", :force => true do |t|
    t.integer  "message_thread_id"
    t.integer  "user_id"
    t.text     "content"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "messages", ["message_thread_id"], :name => "index_messages_on_message_thread_id"
  add_index "messages", ["user_id"], :name => "index_messages_on_user_id"

  create_table "notes", :force => true do |t|
    t.string   "type"
    t.integer  "user_id"
    t.text     "note_contents"
    t.text     "preferences"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.string   "privacy",       :default => "private", :null => false
    t.string   "image"
  end

  add_index "notes", ["user_id"], :name => "index_notes_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "display_name"
    t.string   "email"
    t.string   "avatar"
    t.string   "chosen_locale_code"
    t.string   "chosen_bible_code"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "users", ["provider", "uid"], :name => "index_users_on_provider_and_uid"

  create_table "verses", :force => true do |t|
    t.string   "bible_code"
    t.string   "book_code"
    t.integer  "chapter"
    t.integer  "verse"
    t.text     "content"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "verses", ["bible_code", "book_code", "chapter", "verse"], :name => "index_verses_on_bible_code_and_book_code_and_chapter_and_verse"
  add_index "verses", ["bible_code", "book_code", "chapter"], :name => "index_verses_on_bible_code_and_book_code_and_chapter"

  create_table "virtual_churches", :force => true do |t|
    t.integer  "creator_id"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "virtual_churches", ["creator_id"], :name => "index_virtual_churches_on_creator_id"

end
