class CreateMemberships < ActiveRecord::Migration
  def change
    create_table :memberships do |t|
      t.references :user
      t.references :virtual_church

      t.timestamps
    end
    add_index :memberships, :user_id
    add_index :memberships, :virtual_church_id
  end
end
