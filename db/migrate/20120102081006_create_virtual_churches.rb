class CreateVirtualChurches < ActiveRecord::Migration
  def change
    create_table :virtual_churches do |t|
      t.references :creator
      t.string :name
      t.text :description

      t.timestamps
    end
    add_index :virtual_churches, :creator_id
  end
end
