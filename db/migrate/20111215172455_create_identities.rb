class CreateIdentities < ActiveRecord::Migration
  def change
    create_table :identities do |t|
      t.string :name
      t.string :email
      t.string :password_digest
      
      t.string :forget_password_token
      t.datetime :forget_password_token_created_at
      
      t.timestamps
    end
    
    add_index :identities, :forget_password_token
  end
end
