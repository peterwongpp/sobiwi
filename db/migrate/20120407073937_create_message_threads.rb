class CreateMessageThreads < ActiveRecord::Migration
  def change
    create_table :message_threads do |t|
      t.string  :uniq_id
      t.timestamps
    end
    add_index :message_threads, :uniq_id
  end
end
