class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.string :type
      t.references :user
      t.text :note_contents
      t.text :preferences

      t.timestamps
    end
    add_index :notes, :user_id
  end
end
