class CreateVerses < ActiveRecord::Migration
  def change
    create_table :verses do |t|
      t.string :bible_code
      t.string :book_code
      t.integer :chapter
      t.integer :verse
      t.text :content

      t.timestamps
    end
    
    add_index :verses, [:bible_code, :book_code, :chapter]
    add_index :verses, [:bible_code, :book_code, :chapter, :verse]
  end
end
