class CreateFollowships < ActiveRecord::Migration
  def change
    create_table :followships do |t|
      t.references :user
      t.references :following_user

      t.timestamps
    end
    add_index :followships, :user_id
    add_index :followships, :following_user_id
  end
end
