class AddPrivacyToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :privacy, :string, null: false, default: "private"
  end
end
