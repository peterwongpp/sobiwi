class CreateMessageThreadUsers < ActiveRecord::Migration
  def change
    create_table :message_thread_users do |t|
      t.references :message_thread
      t.references :user

      t.timestamps
    end
    add_index :message_thread_users, :message_thread_id
    add_index :message_thread_users, :user_id
  end
end
