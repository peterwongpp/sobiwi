class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :provider
      t.string :uid
      t.string :display_name
      t.string :email
      t.string :avatar
      t.string :chosen_locale_code
      t.string :chosen_bible_code

      t.timestamps
    end
    add_index :users, [:provider, :uid]
  end
end
