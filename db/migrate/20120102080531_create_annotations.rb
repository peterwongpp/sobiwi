class CreateAnnotations < ActiveRecord::Migration
  def change
    create_table :annotations do |t|
      t.references :note
      t.references :verse
      t.integer :from_atom_index
      t.integer :to_atom_index

      t.timestamps
    end
    add_index :annotations, :note_id
    add_index :annotations, :verse_id
  end
end
