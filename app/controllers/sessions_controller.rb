class SessionsController < ApplicationController
  def new
  end

  def create
    @user = User.from_omniauth(env["omniauth.auth"])
    if @user.chosen_locale_code.blank? || @user.chosen_bible_code.blank?
      @user.assign_attributes({
        chosen_locale_code: session[:chosen_locale_code],
        chosen_bible_code: session[:chosen_bible_code]
      })
      @user.save(validate: false)
    end

    session[:user_id] = @user.id
    session[:chosen_locale_code] = @user.chosen_locale_code
    session[:chosen_bible_code] = @user.chosen_bible_code

    push_flash :success
    redirect_to user_home_url(@user)
  end

  def destroy
    session[:user_id] = nil
    push_flash :success
    redirect_to root_url
  end

  def failure
    push_flash :failure, target: :login_form
    redirect_to :login
  end
end
