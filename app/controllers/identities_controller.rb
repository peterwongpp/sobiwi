class IdentitiesController < ApplicationController
  before_filter :require_not_logged_in!

  def new
    if has_tmp_new_identity?
      @identity = load_tmp_new_identity_from_session
      push_flash :failure, target: :registration_form unless @identity.valid?
    else
      @identity = env['omniauth.identity']

      if @identity.present? && !@identity.valid?
        store_tmp_new_identity_to_session(@identity)
        redirect_to [:new, :identity]
      end

      @identity ||= Identity.new
    end
  end

  private

  def has_tmp_new_identity?
    session[:tmp_new_identity].present?
  end

  def load_tmp_new_identity_from_session
    Identity.new(session.delete(:tmp_new_identity))
  end

  def store_tmp_new_identity_to_session(identity)
    session[:tmp_new_identity] = {
      name: identity.name,
      email: identity.email,
      password: identity.password,
      password_confirmation: identity.password_confirmation
    }
  end
end
