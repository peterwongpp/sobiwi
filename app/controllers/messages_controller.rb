class MessagesController < ApplicationController
  before_filter :require_logged_in!
  before_filter :get_default_message_thread

  def create
    @message = current_user.messages.build(params[:message])
    @message.message_thread = @message_thread

    @message.save
    respond_to do |format|
      format.js
    end
  end

  def destroy
    @message = current_user.messages.find(params[:id])
    @message.destroy
    push_flash :success
    redirect_to :back
  end

  private

  def get_default_message_thread
    @message_thread = current_user.message_threads.find params[:message_thread_id]
  end
end
