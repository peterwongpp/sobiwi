class MessageThreadsController < ApplicationController
  before_filter :require_logged_in!

  layout "membership"

  def index
    @message_threads = MessageThread.threads_involving(current_user)
                                    .delete_if { |message_thread| message_thread.messages.blank? }
                                    .sort_by { |message_thread| message_thread.messages.last.try(:created_at) }
                                    .reverse
  end

  def show
    @message_thread = MessageThread.involving(current_user).find params[:id]
    @users = UserDecorator.decorate @message_thread.users.exclude(current_user)
    @messages = @message_thread.messages.page(params[:page]).per(10)
  end

  def new
    @message_thread = MessageThread.new
    @following_friends = current_user.following_friends
    @friends_following_me = current_user.friends_following_me
  end

  def create
    @message_thread = MessageThread.reuse_or_create(current_user, params[:message_thread])

    redirect_to message_thread_path(@message_thread)
  end
end
