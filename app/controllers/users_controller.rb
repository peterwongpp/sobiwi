class UsersController < ApplicationController
  before_filter :require_logged_in!
  before_filter :get_default_user, only: [:home, :profile, :edit, :update, :destroy]
  before_filter :goto_self_home, only: [:home] # as some actions are in resource :user, so no user id in the URL. That's why we do not goto self home for those actions. e.g. edit, update, destroy

  layout "membership"

  def edit
  end

  def update
    if @user.update_attributes(params[:user])
      push_flash :success
      redirect_to user_profile_path(@user)
    else
      push_flash :warning, target: :update_user_form
      session[:users_update__user] = @user
      redirect_to edit_user_path
    end
  end

  def destroy
    @user.destroy
    session.delete(:user_id)

    redirect_to root_path
  end

  def profile
    @notes = @user.timeline_for_profile.page(params[:page])
  end

  def home
    @notes = @user.timeline_for_home.page(params[:page])
    render layout: "application"
  end

  def follow
    @user = User.find_by_id(params[:id])

    current_user.follow!(@user)

    push_flash(:success, (current_user.following?(@user) ? :"users.follow" : :"users.unfollow"), i18n_options: {name: @user.display_name})
    redirect_to user_profile_path(@user)
  end

  private

  def get_default_user
    @user = session.delete(:users_update__user) || User.find_by_id(params[:user_id]) || current_user
  end

  def goto_self_home
    if @user != current_user
      redirect_to user_home_path(current_user)
    end
  end
end
