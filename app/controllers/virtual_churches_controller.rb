class VirtualChurchesController < ApplicationController
  before_filter :require_logged_in!, only: [:edit, :update, :destroy, :join]
  before_filter :get_default_virtual_church
  before_filter :handle_permissions, only: [:edit, :update, :destroy]

  layout "virtual_church"

  def show
  end

  def edit
  end

  def update
    if @virtual_church.update_attributes(params[:virtual_church])
      push_flash :success
      redirect_to virtual_church_path(@virtual_church)
    else
      push_flash :warning, target: :edit_virtual_church_form
      session[:virtual_churches_update__virtual_church] = @virtual_church
      redirect_to edit_virtual_church_path(@virtual_church)
    end
  end

  def destroy
    @virtual_church.destroy

    redirect_to root_path
  end

  def join
    current_user.join!(@virtual_church)

    push_flash(:success, (current_user.joined?(@virtual_church) ? :"virtual_churches.join" : :"virtual_churches.leave"), i18n_options: {name: @virtual_church.name})
    redirect_to :back
  end

  private

  def get_default_virtual_church
    @virtual_church = session.delete(:virtual_churches_update__virtual_church) || VirtualChurch.find_by_id(params[:id])
  end

  def handle_permissions
    unless current_user.owned?(@virtual_church)
      push_flash :failure, :"virtual_churches.handle_permissions"
      redirect_to root_path
    end
  end
end
