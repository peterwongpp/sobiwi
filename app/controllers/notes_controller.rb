class NotesController < ApplicationController
  before_filter :require_logged_in!

  def create
    @verses = Verse.where(bible_code: params[:bible_code], book_code: params[:book_code], chapter: params[:chapter]).where("verses.verse >= ? AND verses.verse <= ?", params[:from_verse].to_i + 1, params[:to_verse].to_i + 1)

    @note = case params[:modal_chosen]
    when "share"
      NoteShare.new params[:note_share]
    when "note"
      NoteNote.new params[:note_note]
    when "map"
      NoteMap.new params[:note_map].reverse_merge({gmaps: true})
    when "image"
      NoteImage.new params[:note_image]
    when "youtube"
      NoteYoutube.new params[:note_youtube]
    else
      raise "Note Type (#{params[:modal_chosen]}) not yet supported"
    end

    @note.user = current_user

    from_verse = params[:from_verse].to_i + 1
    from_verse_atom = params[:from_verse_atom].to_i
    to_verse = params[:to_verse].to_i + 1
    to_verse_atom = params[:to_verse_atom].to_i

    @verses.each do |verse|
      verse_number = verse.verse
      from_atom_index = verse_number == from_verse ? from_verse_atom : 0
      to_atom_index = verse_number == to_verse ? to_verse_atom : verse.atoms.count - 1

      annotation = @note.annotations.build
      annotation.verse = verse
      annotation.from_atom_index = from_atom_index
      annotation.to_atom_index = to_atom_index
    end

    @note.label_color = params[:note][:label_color]
    @note.privacy = params[:note][:privacy]

    push_flash (@note.save ? :success : :failure)

    redirect_to chapter_path(book_code: params[:book_code], chapter: params[:chapter])
  end

  def destroy
    note = current_user.notes.find params[:id]
    note.destroy
    redirect_to :back
  end
end
