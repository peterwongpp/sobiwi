class PagesController < ApplicationController
  before_filter :goto_home_if_logged_in, only: [:index]
  before_filter :require_logged_in!, only: [:search_user]

  def index
    @timeline_notes = current_user.timeline_for_home.page(params[:page]) if current_user.present?
  end

  def map
    @lat = params[:lat]
    @lng = params[:lng]
    @zoom = params[:zoom]
    render layout: "map"
  end

  def search_user
    @user = User.find_by_email(params[:search_user_email])

    if @user
      push_flash :success
      redirect_to user_profile_path(@user)
    else
      push_flash :failure, i18n_options: {email: params[:search_user_email]}
      redirect_to :back
    end
  end

  def search_church
    @virtual_churches = VirtualChurch.where("name LIKE ?", "%#{params[:search_church_name]}%")
  end
end
