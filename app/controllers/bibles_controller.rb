class BiblesController < ApplicationController
  def show_chapter
    @book = Book.instance_of(chosen_bible.bible_code, params[:book_code])
    @chapter = params[:chapter].to_i
    @verses = VerseDecorator.decorate Verse.where(bible_code: @book.bible.bible_code, book_code: @book.book_code, chapter: @chapter).order("verses.verse ASC")
    @notes = Note.under(@verses).viewable_by(current_user)
  end

  def show_verse
    @verse = Verse.find_by_bible_code_and_book_code_and_chapter_and_verse(chosen_bible.bible_code, params[:book_code], params[:chapter].to_i, params[:verse].to_i)
    @notes = Note.under(@verse).viewable_by(current_user).order("notes.created_at DESC").page(params[:page])
  end
end
