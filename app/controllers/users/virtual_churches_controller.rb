class Users::VirtualChurchesController < ApplicationController
  before_filter :require_logged_in!

  layout "membership"

  def index
    @owned_virtual_churches = current_user.owned_virtual_churches
    @memberships = current_user.memberships
  end

  def new
    @virtual_church = session.delete(:virtual_church_create_update) || current_user.owned_virtual_churches.build
  end

  def create
    @virtual_church = current_user.owned_virtual_churches.build(params[:virtual_church])

    if @virtual_church.save
      push_flash :success
      redirect_to user_virtual_churches_path
    else
      push_flash :warning, target: :church_form
      session[:virtual_church_create_update] = @virtual_church
      redirect_to new_user_virtual_church_path
    end
  end

  def edit
    @virtual_church = session.delete(:virtual_church_create_update) || current_user.owned_virtual_churches.find(params[:id])
  end

  def update
    @virtual_church = current_user.owned_virtual_churches.find params[:id]

    if @virtual_church.update_attributes(params[:virtual_church])
      push_flash :success
      redirect_to user_virtual_churches_path
    else
      push_flash :warning, target: :church_form
      session[:virtual_church_create_update] = @virtual_church
      redirect_to edit_user_virtual_church_path(@virtual_church)
    end
  end

  def destroy
    @virtual_church = current_user.owned_virtual_churches.find params[:id]
    @virtual_church.memberships.destroy_all
    @virtual_church.destroy

    push_flash :success
    redirect_to user_virtual_churches_path
  end
end
