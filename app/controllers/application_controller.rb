class ApplicationController < ActionController::Base
  protect_from_forgery

  helper_method :current_user, :logged_in?

  helper_method :available_locales, :chosen_locale
  helper_method :available_bibles, :chosen_bible

  before_filter :choose_locale, :choose_bible

  def current_user
    return @current_user if @current_user.present?
    @current_user = User.find_by_id(session[:user_id])
  end

  def logged_in?
    !current_user.nil?
  end

  def available_locales
    Locale.all
  end
  def chosen_locale
    @chosen_locale ||= Locale.instance_of(session[:chosen_locale_code])
  end

  def available_bibles
    Bible.all
  end
  def chosen_bible
    @chosen_bible ||= Bible.instance_of(session[:chosen_bible_code])
  end

  def require_logged_in!
    unless logged_in?
      push_flash :failure, :"application.require_logged_in"
      redirect_to root_path
    end
  end

  def require_not_logged_in!
    if logged_in?
      push_flash :failure, :"application.require_not_logged_in"
      redirect_to root_path
    end
  end

  def goto_home_if_logged_in
    if logged_in?
      redirect_to user_home_path(current_user)
    end
  end

  private

  def choose_locale
    session[:chosen_locale_code] ||= DEFAULT_LOCALE

    I18n.locale = session[:chosen_locale_code]
    if params[:locale] && Locale.has?(params[:locale])
      locale = params[:locale].to_sym

      I18n.locale = locale
      session[:chosen_locale_code] = locale
      if logged_in?
        current_user.assign_attributes({chosen_locale_code: locale}, without_protection: true)
        current_user.save(validate: false)
      end

      begin
        redirect_to :back
      rescue
        redirect_to :root
      end
    end
  end

  def choose_bible
    session[:chosen_bible_code] ||= DEFAULT_BIBLE

    if params[:choose_bible] && Bible.has?(params[:choose_bible])
      bible_code = params[:choose_bible].to_sym

      session[:chosen_bible_code] = bible_code
      if logged_in?
        current_user.assign_attributes({chosen_bible_code: bible_code}, without_protection: true)
        current_user.save(validate: false)
      end

      begin
        redirect_to :back
      rescue
        redirect_to :root
      end
    end
  end
end
