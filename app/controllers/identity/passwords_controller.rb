class Identity::PasswordsController < ApplicationController
  before_filter :get_default_identity, :redirect_back_to_new_if_token_invalid, only: [:edit, :update]

  def new
  end

  def create
    if (@identity = Identity.find_by_email(params[:email])).nil?
      push_flash :failure, target: :reset_password_form
    else
      @identity.generate_forget_password_token!
      push_flash :success, target: :reset_password_form
    end

    redirect_to new_identity_password_path(email: params[:email])
  end

  def edit
  end

  def update
    @identity.password = params[:password]
    @identity.password_confirmation = params[:password_confirmation]

    if @identity.save
      @identity.deactivate_forget_password_token!
      auth = {
        provider: "identity",
        uid: @identity.id
      }
      user = User.from_omniauth(auth)
      session[:user_id] = user.id
      push_flash :success
      redirect_to :root
    else
      session[:identity_passwords_update__identity] = @identity
      push_flash :failure, target: :reset_password_form
      redirect_to edit_identity_password_url(id: @identity.forget_password_token)
    end
  end

  private

  def get_default_identity
    @identity = session.delete(:identity_passwords_update__identity) || Identity.find_by_forget_password_token(params[:id])
  end

  def redirect_back_to_new_if_token_invalid
    if @identity.nil?
      push_flash :token_not_exist, :"identity.passwords.update", target: :reset_password_form
      redirect_to new_identity_password_path
    elsif @identity.forget_password_expired?
      push_flash :token_expired, :"identity.passwords.update", target: :reset_password_form
      redirect_to new_identity_password_path
    end
  end
end
