class Locale
  attr_reader :locale_code, :name
  
  # singleton!
  def initialize(locale_code)
    @locale_code = locale_code.to_sym
    @name = I18n.t("locales.#{locale_code}")
  end
  private_class_method :new
  def self.instance_of(locale_code)
    $locale_instances ||= {}
    key = locale_code.to_sym
    return $locale_instances[key] if $locale_instances[key].present?
    
    $locale_instances[key] = new(locale_code)
  end
  # end of singleton
  
  class << self
    def all
      LOCALES.map { |locale_code| instance_of(locale_code) }
    end
    
    def has?(locale_code)
      LOCALES.include?(locale_code.to_s)
    end
  end
end