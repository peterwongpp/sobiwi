class Identity < OmniAuth::Identity::Models::ActiveRecord
  validates :name, presence: true
  validates :email, uniqueness: true, format: /^[^@\s]+\@([-a-z0-9]+\.)+[a-z0-9]{2,4}$/i
  validates :password, length: { minimum: 6 }, if: :need_validate_password?

  def generate_forget_password_token!
    update_attributes({
      forget_password_token: Digest::SHA1.hexdigest("-~#{name}.=.#{email}*&^#{Time.now}sdfk"),
      forget_password_token_created_at: DateTime.current
    }, without_protection: true)

    IdentityMailer.forget_password(self).deliver
  end

  def forget_password_expired?
    !forget_password_token_created_at.blank? && 2.days.ago > forget_password_token_created_at
  end

  def deactivate_forget_password_token!
    update_attributes({
      forget_password_token: nil,
      forget_password_token_created_at: nil
    }, without_protection: true)
  end

  private

  def need_validate_password?
    !persisted? || !password.nil? || !password_confirmation.nil?
  end
end
