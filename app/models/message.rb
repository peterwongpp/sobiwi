class Message < ActiveRecord::Base
  belongs_to :message_thread
  belongs_to :user

  scope :latest, lambda { |n| order("messages.created_at DESC").limit(n) }

  validates :content, presence: true
end
