class User < ActiveRecord::Base
  mount_uploader :avatar, AvatarUploader

  has_many :notes, dependent: :destroy

  has_many :followships, foreign_key: "user_id", dependent: :destroy # the users I am following
  has_many :following_friends, through: :followships, source: :following_user
  has_many :passive_followships, class_name: "Followship", foreign_key: "following_user_id", dependent: :destroy # the users who following me
  has_many :friends_following_me, through: :passive_followships, source: :user

  has_many :owned_virtual_churches, class_name: "VirtualChurch", foreign_key: "creator_id", dependent: :destroy
  has_many :memberships, dependent: :destroy

  has_many :message_thread_users
  has_many :message_threads, through: :message_thread_users
  has_many :messages

  scope :exclude, lambda { |user| where("users.id != ?", user.id) }

  validates :display_name, presence: true
  validates :email, uniqueness: { scope: :provider, case_sensitive: true }, format: /^[^@\s]+\@([-a-z0-9]+\.)+[a-z0-9]{2,4}$/i, if: :need_validating_email

  after_save :update_identity_attributes
  after_destroy :destroy_identity

  def self.from_omniauth(auth)
    find_by_provider_and_uid(auth["provider"], auth["uid"]) || create_with_omniauth(auth)
  end

  def self.create_with_omniauth(auth)
    auth["info"] ||= {}
    auth["info"]["name"] ||= ""
    auth["info"]["email"] ||= ""

    create do |user|
      user.provider = auth["provider"]
      user.uid = auth["uid"]
      user.display_name = auth["info"]["name"]
      user.email = auth["info"]["email"] || auth["raw"]["email"]
    end
  end

  def timeline_for_home
    friend_ids = followships.collect(&:following_user_id)
    church_ids = owned_virtual_churches.collect(&:id) + memberships.collect(&:virtual_church_id)
    church_friend_ids = Membership.select(:user_id).where(virtual_church_id: church_ids).collect(&:user_id)
    user_ids = ([id] + friend_ids + church_friend_ids).uniq

    Note.where(user_id: user_ids).viewable_by(self).uniq.order("notes.created_at DESC")
  end
  def timeline_for_profile
    Note.where(user_id: id).order("notes.created_at DESC")
  end

  def following?(user)
    return false unless user
    return false if user == self
    return false unless followships.find_by_following_user_id(user.id)

    true
  end
  def follow!(user)
    return if user == self

    if following?(user)
      followships.find_by_following_user_id(user.id).destroy
    else
      followships.create(following_user: user)
    end
  end

  def joined?(virtual_church)
    return false unless virtual_church
    return false if owned?(virtual_church)
    return false unless memberships.find_by_virtual_church_id(virtual_church.id)

    true
  end
  def join!(virtual_church)
    return unless virtual_church

    if joined?(virtual_church)
      memberships.find_by_virtual_church_id(virtual_church.id).destroy
    else
      memberships.create({
        virtual_church: virtual_church
      })
    end
  end
  def owned?(virtual_church)
    !virtual_church.nil? && virtual_church.creator_id == id
  end

  private

  def need_validating_email
    !email.blank?
  end

  def update_identity_attributes
    if provider == "identity"
      identity = Identity.find(uid)

      identity.email = email
      identity.name = display_name

      identity.save
    end
  end

  def destroy_identity
    if provider == "identity"
      identity = Identity.find(uid)
      identity.destroy
    end
  end
end
