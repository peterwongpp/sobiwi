class Note < ActiveRecord::Base
  belongs_to :user
  has_many :annotations, dependent: :destroy

  store :preferences, accessors: [ :label_color ]

  attr_accessible :label_color

  scope :under, lambda { |verses|
    if verses.blank?
      where("1 = 0")
    else
      verse_ids = verses.respond_to?(:map) ? verses.map(&:id) : [verses.id]
      joins(:annotations).where("annotations.verse_id in (?)", verse_ids).uniq
    end
  }
  scope :viewable_by, lambda { |user|
    return where("1 = 0") unless user

    user_church_ids = (user.owned_virtual_churches.map(&:id) + user.memberships.map(&:virtual_church_id)).uniq

    joins(user: [:passive_followships, :memberships]).where("
      (notes.user_id = ?) OR
      ((notes.privacy = ? OR notes.privacy = ?) AND followships.user_id = ?) OR
      (notes.privacy = ? AND memberships.virtual_church_id in (?))
    ", user.id, "friends", "churches", user.id, "churches", user_church_ids)
  }

  def label_color
    preferences[:label_color] || LabelColor::ALL.values.first
  end

  def verse_name
    first = annotations.first.verse
    last = annotations.last.verse

    if first == last
      "#{first.book.short_name} #{first.chapter}:#{first.verse}"
    else
      "#{first.book.short_name} #{first.chapter}:#{first.verse}-#{last.verse}"
    end
  end
  def verse_content
    result = []
    first_verse = annotations.first.verse
    first_atom_index = annotations.first.from_atom_index
    last_verse = annotations.last.verse
    last_atom_index = annotations.last.to_atom_index

    result << "..." unless first_verse.first_atom?(first_atom_index)
    annotations.each do |annotation|
      result << annotation.verse.content[annotation.from_atom_index, annotation.to_atom_index-annotation.from_atom_index]
    end
    result << "..." unless last_verse.last_atom?(last_atom_index)

    result.join(" ")
  end

  def to_partial_path
    "shared/notes/#{self.type.underscore.gsub(/^note_/, "")}"
  end
end
