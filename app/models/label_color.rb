class LabelColor
  ALL = {
    yellow: "yellow",
    pink: "pink",
    blue: "#9aF"
  }

  attr_accessor :name, :value

  class << self
    def all
      ALL.values.map { |value| LabelColor.new(value) }
    end
  end

  def initialize(value)
    @name = "<div style=\"background-color: #{value}; width: 32px; height: 32px;\">&nbsp;</div>".html_safe
    @value = value
  end
end