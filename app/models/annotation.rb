class Annotation < ActiveRecord::Base
  belongs_to :note
  belongs_to :verse

  attr_protected :note, :note_id, :verse, :verse_id
  attr_protected :created_at, :updated_at
  attr_protected :from_atom_index, :to_atom_index
end
