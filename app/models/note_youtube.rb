class NoteYoutube < Note
  store :note_contents, accessors: [ :title, :youtube_url, :youtube_id ]

  attr_accessible :title, :youtube_url

  before_save :retrieve_youtube_id

  def embed(width=640, height= 360)
    "<iframe width=\"#{width}\" height=\"#{height}\" src=\"http://www.youtube.com/embed/#{youtube_id}\" frameborder=\"0\" allowfullscreen></iframe>"
  end

  private

  def retrieve_youtube_id
    matches = youtube_url.match /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/
    update_attribute(:youtube_id, matches[7]) unless  matches.nil? || matches[7].length != 11 || youtube_id == matches[7]
  end
end
