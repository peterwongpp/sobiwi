class Bible
  attr_reader :bible_code
  attr_reader :name, :short_name
  attr_reader :testaments
  
  # singleton!
  def initialize(bible_code)
    @bible_code = bible_code.to_sym
  end
  private_class_method :new
  def self.instance_of(bible_code)
    $bible_instances ||= {}
    key = bible_code.to_sym
    return $bible_instances[key] if $bible_instances[key].present?
    
    $bible_instances[key] = new(bible_code)
  end
  # end of singleton
  
  class << self
    def all
      BIBLES.map { |bible_code| instance_of(bible_code) }
    end
    
    def has?(bible_code)
      BIBLES.include?(bible_code.to_s)
    end
  end
  
  def name
    I18n.t("bibles.#{bible_code}.name")
  end
  
  def short_name
    I18n.t("bibles.#{bible_code}.short_name")
  end
  
  def testaments
    [:old, :new].map { |testament_code| Testament.instance_of(bible_code, testament_code) }
  end
end