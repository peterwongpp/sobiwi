class Privacy
  ALL = {
    private: "private",
    friends: "friends",
    churches: "churches"
  }

  attr_accessor :name, :value # name: to be displayed, value: to be stored

  class << self
    def all
      ALL.keys.map { |key| Privacy.new(key, ALL[key]) }
    end
  end

  def initialize(name, value)
    @name = I18n.t("activerecord.attributes.privacy.#{name}")
    @value = value
  end
end
