class VirtualChurch < ActiveRecord::Base
  belongs_to :creator, class_name: "User"
  has_many :memberships, dependent: :destroy

  validates :name, presence: true, uniqueness: true
end
