class NoteNote < Note
  store :note_contents, accessors: [ :title, :content ]

  attr_accessible :title, :content
end