class NoteShare < Note
  store :note_contents, accessors: [ :content ]

  attr_accessible :content
end