class Verse < ActiveRecord::Base
  has_many :annotations, dependent: :destroy

  def bible
    Bible.instance_of(bible_code)
  end

  def testament
    Testament.instance_of(bible_code, book.testament.testament_code)
  end

  def book
    Book.instance_of(bible_code, book_code)
  end

  def name
    "#{book.short_name} #{chapter}:#{verse}"
  end

  def in_bible_version(another_bible_code)
    Verse.where(bible_code: another_bible_code, book_code: book_code, chapter: chapter, verse: verse).first
  end

  def verse_number
    verse
  end
  def verse_number=(value)
    verse = value
  end

  def to_verse_path_hash
    {
      book_code: book_code,
      chapter: chapter,
      verse: verse
    }
  end

  def atoms
    @atoms ||= self.content.split(//).each_with_object([]) do |atom, array|
      tmp = {
        atom: atom,
        index: array.count
      }
      array << tmp
    end
  end
  def first_atom?(atom_index)
    atom_index == 0
  end
  def last_atom?(atom_index)
    atom_index == atoms.count-1
  end
end
