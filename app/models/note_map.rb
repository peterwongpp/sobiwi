class NoteMap < Note
  acts_as_gmappable
  store :note_contents, accessors: [ :title, :address , :latitude, :longitude, :gmaps, :zoom ]

  attr_accessible :title, :address, :latitude, :longitude, :gmaps, :zoom

  def gmaps4rails_address
    address
  end

  def zoom
    note_contents[:zoom].to_i
  end
  def zoom=(val)
    note_contents[:zoom] = val.to_i
  end

  def latitude
    note_contents[:latitude].to_s
  end
  def latitude=(val)
    note_contents[:latitude] = val.to_s
  end

  def longitude
    note_contents[:longitude].to_s
  end
  def longitude=(val)
    note_contents[:longitude] = val.to_s
  end
end
