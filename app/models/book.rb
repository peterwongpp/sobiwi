class Book
  attr_reader :bible_code, :testament_code, :book_code
  attr_reader :name, :short_name
  attr_reader :bible, :testament

  # singleton!
  def initialize(bible_code, testament_code, book_code)
    @bible_code = bible_code.to_sym
    @testament_code = testament_code.to_sym
    @book_code = book_code.to_sym
  end
  private_class_method :new
  def self.instance_of(bible_code, book_code)
    $book_instances ||= {}
    key = :"#{bible_code}-#{book_code}"
    return $book_instances[key] if $book_instances[key].present?

    testament_code = if BOOKS[:new].include?(book_code.to_s)
      :new
    elsif BOOKS[:old].include?(book_code.to_s)
      :old
    end

    $book_instances[key] = new(bible_code, testament_code, book_code)
  end
  # end of singleton

  def name
    I18n.t("bibles.#{bible_code}.testaments.#{testament_code}.books.#{book_code}.name")
  end

  def short_name
    I18n.t("bibles.#{bible_code}.testaments.#{testament_code}.books.#{book_code}.short_name")
  end

  def chapters_count
    Verse.where(bible_code: bible_code, book_code: book_code).group(:chapter).count.count
  end

  def verses_in_chapter(n)
    Verse.where(bible_code: bible_code, book_code: book_code, chapter: n).order("verse ASC")
  end

  def bible
    Bible.instance_of(bible_code)
  end

  def testament
    Testament.instance_of(bible_code, testament_code)
  end

  def to_chapter_path_hash(chapter=1)
    {
      book_code: book_code,
      chapter: chapter
    }
  end

  def to_verse_path_hash(chapter=1, verse=1)
    {
      book_code: book_code,
      chapter: chapter,
      verse: verse
    }
  end
end