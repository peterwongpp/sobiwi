class Testament
  attr_reader :bible_code, :testament_code
  attr_reader :name
  attr_reader :bible, :books

  # singleton!
  def initialize(bible_code, testament_code)
    @bible_code = bible_code.to_sym
    @testament_code = testament_code.to_sym
  end
  private_class_method :new
  def self.instance_of(bible_code, testament_code)
    $testament_instances ||= {}
    key = :"#{bible_code}-#{testament_code}"
    return $testament_instances[key] if $testament_instances[key].present?

    $testament_instances[key] = new(bible_code, testament_code)
  end
  # end of singleton

  def name
    I18n.t("bibles.#{bible_code}.testaments.#{testament_code}.name")
  end

  def bible
    Bible.instance_of(bible_code)
  end

  def books
    BOOKS[testament_code].map { |book_code| Book.instance_of(bible_code, book_code) }
  end
end