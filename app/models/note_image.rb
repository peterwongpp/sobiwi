class NoteImage < Note
  store :note_contents, accessors: [ :title ]

  attr_accessible :title, :image

  mount_uploader :image, NoteImageUploader
end
