class MessageThread < ActiveRecord::Base
  has_many :message_thread_users
  has_many :users, through: :message_thread_users

  has_many :messages

  scope :involving, lambda { |user|
    joins(:message_thread_users).where("message_thread_users.user_id = ?", user.id)
  }

  after_create :generate_uniq_id

  class << self
    def threads_involving(current_user)
      thread_ids = MessageThreadUser.where(user_id: current_user.id).collect(&:message_thread_id)
      where(id: thread_ids)
    end

    def reuse_or_create(current_user, message_thread_params)
      user_ids = ([current_user.id] + message_thread_params[:user_ids]).uniq.delete_if(&:blank?).map(&:to_i)
      message_content = message_thread_params[:messages][:content]

      message_thread = MessageThread.where(uniq_id: user_ids.sort.join("--")).first

      if message_thread.nil?
        message_thread = MessageThread.new

        # create mesasge_thread_users
        user_ids.each do |user_id|
          message_thread.message_thread_users.build(user_id: user_id)
        end
      end

      # create initial message
      unless message_content.blank?
        message_thread.messages.build(user_id: current_user.id, content: message_content)
      end

      message_thread.save!
      message_thread
    end
  end

  private

  def generate_uniq_id
    update_attribute(:uniq_id, message_thread_users.map(&:user_id).sort.join("--"))
  end
end
