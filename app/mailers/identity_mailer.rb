class IdentityMailer < ActionMailer::Base
  default from: "no-reply@sbw.herokuapp.com"

  def forget_password(identity)
    @display_name = identity.name
    @url = url_for(edit_identity_password_url(id: identity.forget_password_token))

    mail(
      to: identity.email,
      subject: "[Sobiwi] Forgot password"
    )
  end
end
