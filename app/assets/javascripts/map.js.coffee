# This is a manifest file that'll be compiled into including all the files listed below.
# Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
# be included in the compiled file accessible from http://example.com/assets/application.js
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# the compiled file.
#

#= require google-analytics
#= require gmaps4rails/googlemaps.js

#= require fixes

#= require ./jquery/jquery.1.7.1.min
#= require ./jquery/jquery-ui-1.8.16.custom.min
#= require jquery_ujs

#= require_self
