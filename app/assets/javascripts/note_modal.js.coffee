jQuery(($) ->
  class NoteModal
    constructor: () ->
      @obj = $("#highlighter-modal")
    modal: (options) ->
      @obj.modal(options)
    updateVerse: (label, content) ->
      @obj.find(".modal-verse").html("""
      <blockquote>
         #{content}<br>
         <cite style="line-height: 24px"><span class="label notice verse-label">#{label}</span></cite>
      </blockquote>
      """)
    updateBody: (bible_code, book_code, chapter, from_verse, from_verse_atom, to_verse, to_verse_atom) ->
      @obj.find(".modal-attrs").html("""
        <input type="hidden" name="bible_code" value="#{bible_code}" />
        <input type="hidden" name="book_code" value="#{book_code}" />
        <input type="hidden" name="chapter" value="#{chapter}" />
        <input type="hidden" name="from_verse" value="#{from_verse}" />
        <input type="hidden" name="from_verse_atom" value="#{from_verse_atom}" />
        <input type="hidden" name="to_verse" value="#{to_verse}" />
        <input type="hidden" name="to_verse_atom" value="#{to_verse_atom}" />
      """)
    getObject: () ->
      @obj

  window.NoteModal = NoteModal
)