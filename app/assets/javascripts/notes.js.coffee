jQuery(($) ->
  class Note
    constructor: (@note_id, @note_type, @label_color, @annotations) ->
      @annotation_length = 0
      @annotation_length += annotation.to_atom_index - annotation.from_atom_index for annotation in @annotations
    window.Note = Note

  class Annotation
    constructor: (@annotation_id, @verse_id, @verse_number, @from_atom_index, @to_atom_index) ->
  window.Annotation = Annotation

  notes = []
  window.notes = notes

  HIGHLIGHTING_GROUP = -1
  HIGHLIGHTING_GROUPS = {}
  highlight = (notes) ->
    notes = quicksort(notes, "ASC")
    $.each(notes, (index, note) ->
      $.each(note.annotations, (index, annotation) ->
        $spans = retrieveSpans(annotation.verse_number, annotation.from_atom_index, annotation.to_atom_index)
        if HIGHLIGHTING_GROUPS[note.note_id] is undefined
          HIGHLIGHTING_GROUPS[note.note_id] = $spans
        else
          HIGHLIGHTING_GROUPS[note.note_id].add($spans)

        $spans
          .data("original-color", note.label_color)
          .css("background-color", note.label_color)
          .each(() ->
            $span = $(this)
            groups = $span.data("highlight-groups") || []
            groups[groups.length] = note.note_id
            $span.data("highlight-groups", groups)
            colors_by_groups = $span.data("highlight-colors") || {}
            colors_by_groups[note.note_id] = note.label_color
            $span.data("colors-by-groups", colors_by_groups)
            $span.on({
              mouseenter: () ->
                $this = $(this)
                highlight_groups = $this.data("highlight-groups")
                return unless highlight_groups?
                return if highlight_groups.indexOf(HIGHLIGHTING_GROUP) != -1
                HIGHLIGHTING_GROUP = $this.data("highlight-groups")[0]
                mouseOverGroup(HIGHLIGHTING_GROUP, $this.data("colors-by-groups")[note.note_id])
            })
          )
      )
    )
  window.highlight = highlight
  retrieveSpans = (verse_number, from_atom_index, to_atom_index) ->
    $("li[data-verse='#{verse_number}'] span").slice(from_atom_index, to_atom_index) # to_atom_index + 1??
  mouseOverGroup = (note_id, color) ->
    HIGHLIGHTING_GROUPS[note_id].css({
      "background-color": color
    })
  undrawBorder = (note_id) ->

  quicksort = (array, order="ASC") ->
    return array if array.length < 2

    pivot = array.shift()
    left = []
    right = []
    for note in array
      if order is "DESC"
        if note.annotation_length <= pivot.annotation_length then left[left.length] = note else right[right.length] = note
      else
        if note.annotation_length > pivot.annotation_length then left[left.length] = note else right[right.length] = note

    return quicksort(left).concat([pivot], quicksort(right))

)