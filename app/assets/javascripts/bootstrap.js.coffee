jQuery(($) ->
  $("a[rel=popover]").popover()
  $(".tooltip").tooltip()
  $("a[rel=tooltip]").tooltip()
  $("a[data-tooltip=true]").tooltip()
  $(".alert-message").alert()
)