jQuery(($) ->
  $KEEP_TRACK = false
  $FROM = $TO = []
  $LAST_SELECTED_ATOMS = []
  $SELECTED_ATOMS = []

  methods = {
    init: (options) ->
      this.find("li[data-verse] span").on({
        mousedown: (e) ->
          $this = $(this)
          $KEEP_TRACK = true
          methods["mark_start"].apply($this, [$this])

        mouseenter: (e) ->
          $this = $(this)
          methods["marking"].apply($this, [$this]) if $KEEP_TRACK
      })
      $("body").on({
        mouseup: (e) ->
          $KEEP_TRACK = false
      })

      $(this).disableSelection() # TODO: this depends on jQuery UI, change it back

      this

    get_identifier: (verse_atom) ->
      $va = $(verse_atom)
      $verse = $va.parent("li[data-verse]")
      $chapter = $va.parents("ul.highlightable")

      atom = $va.data("atom")
      verse = $verse.data("verse")
      chapter = $chapter.data("chapter")
      book = $chapter.data("book")
      bible = $chapter.data("bible")

      [bible, book, chapter, verse, atom]

    mark_start: (from) ->
      $SELECTED_ATOMS = []
      methods["deselect_last_selected_atoms"].apply(this)
      $FROM = methods["get_identifier"].apply(this, [from])

    marking: (to) ->
      $TO = methods["get_identifier"].apply(this, [to])
      $SELECTED_ATOMS = []
      return if $FROM[0] isnt $TO[0] or $FROM[1] isnt $TO[1] or $FROM[2] isnt $TO[2] # cannot highlight across bibles, books or even chapters. Only across verses is allowed.
      return if $FROM[3] is $TO[3] and $FROM[4] is $TO[4] # no need to highlight if nothing is selected

      chapter = $("ul[data-bible='"+$FROM[0]+"'][data-book='"+$FROM[1]+"'][data-chapter='"+$FROM[2]+"']")
      [real_from, real_to] = if $FROM[3] < $TO[3] or ($FROM[3] is $TO[3] and $FROM[4] < $TO[4]) then [$FROM, $TO] else [$TO, $FROM]
      highlighting_forward = if $FROM is real_from then true else false

      for verse_num in [real_from[3]..real_to[3]]
        verse = chapter.find("li[data-verse='"+verse_num+"']")

        verse_max_atom = verse.data("max-atom")
        start_atom = if verse_num is real_from[3] then real_from[4] else 1
        end_atom = if verse_num is real_to[3] then real_to[4] else verse_max_atom

        for verse_atom in [start_atom..end_atom]
          atom = verse.find("span[data-atom='"+verse_atom+"']")
          $SELECTED_ATOMS[$SELECTED_ATOMS.length] = atom

        methods["deselect_last_selected_atoms"].apply(this)
        methods["handle_highlighted_atoms"].apply(this)
        $LAST_SELECTED_ATOMS = $SELECTED_ATOMS

    handle_highlighted_atoms: () ->
      for atom in $SELECTED_ATOMS
        atom.css("background-color", "#333")

    deselect_last_selected_atoms: () ->
      for atom in $LAST_SELECTED_ATOMS
        atom.css("background-color", "#fff") unless atom in $SELECTED_ATOMS
  }

  $.fn.textHighlightable = (method) ->
    if methods[method]
      methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    else if typeof method is 'object' or !method
      methods.init.apply(this, arguments)
    else
      $.error('Method' + method + ' does not exist on jQuery.textHighlightable');
)
