jQuery(($) ->
  UL = $(".highlightable")
  DISABLED = false
  KEEP_TRACK = false
  FROM = TO = LAST_TO = [null, null]
  DEFAULT_COLOR = "white"
  DEFAULT_HIGHLIGHTED_COLOR = "#999"

  MODAL = new window.NoteModal
  MODAL.modal({backdrop: true, keyboard: true, show: false})
  window.MODAL = MODAL
  updateModal = (from, to) ->
    verse_content = getVerseContent(from, to)
    verse_label = getVerseLabel(from, to)
    MODAL.updateVerse(verse_label, verse_content)
    bible_code = UL.data("bible")
    book_code = UL.data("book")
    chapter = UL.data("chapter")
    from_verse = from[0]
    from_verse_atom = from[1]
    to_verse = to[0]
    to_verse_atom = to[1]
    MODAL.updateBody(bible_code, book_code, chapter, from_verse, from_verse_atom, to_verse, to_verse_atom)
  getVerseContent = (from, to) ->
    from_li_index = from[0]
    to_li_index = to[0]
    rows = ""

    for i in [from_li_index..to_li_index]
      cols = ""
      from_span_index = if i is from_li_index then from[1] else 0
      to_span_index = if i is to_li_index then to[1] else null

      the_li = UL.find(":nth-child(#{i+1})}")
      spans = if to_span_index is null then the_li.children().slice(from_span_index+1) else the_li.children().slice(from_span_index+1, to_span_index+1)
      spans.each(() ->
        cols = "#{cols}#{$(this).text()}"
      )
      rows = "#{rows} #{cols}"
    rows
  getVerseLabel = (from, to) ->
    verse_number = if from[0] is to[0] then from[0]+1 else "#{from[0]+1}-#{to[0]+1}"
    "#{UL.data("chapter")}:#{verse_number}"

  activateHighlighter = () ->
    DISABLED = false
    $("ul.highlightable").disableSelection()
    $("body").append(MODAL.getObject())

    $("li[data-verse] span").on({
      mousedown: () ->
        return if DISABLED
        clearPreviouslySelected()
        KEEP_TRACK = true
        FROM = TO = get_indexes(this)
        LAST_TO = FROM
        update_selected(this)

      mouseenter: () ->
        drawHighlightedBorder(this)
        return if DISABLED
        if KEEP_TRACK
          LAST_TO = TO
          TO = get_indexes(this)
          update_selected(this)
      mouseout: () ->
        undrawHighlightedBorder(this)
    })
    $("body").on({
      mouseup: () ->
        return if DISABLED
        if KEEP_TRACK
          KEEP_TRACK = false
          [from, to] = organize(FROM, TO)
          unless from is to
            updateModal(from, to)
            MODAL.modal("show")
    })
  window.activateHighlighter = activateHighlighter
  deactivateHighlighter = () ->
    DISABLED = true
    $("ul.highlightable").enableSelection()
    MODAL.getObject().remove()
  window.deactivateHighlighter = deactivateHighlighter
  highlighterActivated = () ->
    !DISABLED
  window.highlighterActivated = highlighterActivated

  clearPreviouslySelected = () ->
    [from, to] = organize(FROM, TO)
    dehighlightSelected(from, to)

  get_indexes = (span) ->
    $span = $(span)
    $li = $span.parent()
    li_index = UL.children().index($li)
    span_index = $li.children().index($span) - 1 # because there is a label (<a> tag) before every spans inside a li tag
    [li_index, span_index]

  update_selected = (span) ->
    [highlight_from, highlight_to] = organize(FROM, TO)

    if compare(FROM, LAST_TO) is 1 and compare(TO, LAST_TO) is 1
      [restore_from, restore_to] = [LAST_TO, highlight_from]
    else if compare(FROM, LAST_TO) is -1 and compare(TO, LAST_TO) is -1
      [restore_from, restore_to] = [highlight_to, LAST_TO]
    else
      [restore_from, restore_to] = [null, null]

    highlightSelected(highlight_from, highlight_to)
    dehighlightSelected(restore_from, restore_to) unless restore_from is null

  highlightSelected = (from, to) ->
    from_li_index = from[0]
    to_li_index = to[0]

    for i in [from_li_index..to_li_index]
      from_span_index = if i is from_li_index then from[1] else 0
      to_span_index = if i is to_li_index then to[1] else null

      the_li = UL.find(":nth-child(#{i+1})}")
      spans = if to_span_index is null then the_li.children().slice(from_span_index+1) else the_li.children().slice(from_span_index+1, to_span_index+1)
      spans.css("background-color", DEFAULT_HIGHLIGHTED_COLOR )

  dehighlightSelected = (from, to) ->
    from_li_index = from[0]
    to_li_index = to[0]

    for i in [from_li_index..to_li_index]
      from_span_index = if i is from_li_index then from[1] else 0
      to_span_index = if i is to_li_index then to[1] else null

      the_li = UL.find(":nth-child(#{i+1})}")
      spans = if to_span_index is null then the_li.children().slice(from_span_index+1) else the_li.children().slice(from_span_index+1, to_span_index+1)
      spans.each((index, element) ->
        $this = $(this)
        $this.css("background-color", ($this.data("original-color") || DEFAULT_COLOR ))
      )

  drawHighlightedBorder = (span) ->
    $span = $(span)
  undrawHighlightedBorder = (span) ->

  organize = (loc1, loc2) ->
    if compare(loc1, loc2) is -1 then [loc1, loc2] else [loc2, loc1]

  compare = (loc1, loc2) ->
    if loc1[0] > loc2[0] or (loc1[0] is loc2[0] and loc1[1] > loc2[1])
      return 1
    else if loc1[0] < loc2[0] or (loc1[0] is loc2[0] and loc1[1] < loc2[1])
      return -1
    else
      return 0
)
