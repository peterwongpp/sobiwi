if !Array.indexOf
  Array.prototype.indexOf = (obj) ->
    for i in [0...this.length]
      if this[i] == obj
        return i
    return -1
