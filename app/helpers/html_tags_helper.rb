module HtmlTagsHelper
  def sign_in_out_link
    if logged_in?
      link_to t("words.signout"), logout_path
    else
      link_to t("words.signin"), login_path
    end
  end

  def registration_profile_link
    if logged_in?
      link_to t("words.profile"), user_profile_path(current_user)
    else
      link_to t("words.register"), [:new, :identity]
    end
  end

  def copyright
    I18n.t("site.copyright", :name => link_to(I18n.t("site.name"), :root), :year => Time.now.year).html_safe
  end

  def paginate_chapters(book, current_chapter)
    total_pages = book.chapters_count
    current_page = current_chapter

    lower_bound = current_page - 4
    upper_bound = current_page + 4

    if lower_bound < 1
      abs = -lower_bound + 1

      lower_bound += abs
      upper_bound += abs
    end

    upper_bound = total_pages if upper_bound > total_pages

    content_tag :div, class: "pagination" do
      content_tag :ul do
        concat paginate_chapter_page(paginate_chapter_content(book, 1, I18n.t("pagination.first")), {classes: "first", disabled: (current_chapter == 1)})

        (lower_bound..upper_bound).each do |chapter|
          concat paginate_chapter_page(paginate_chapter_content(book, chapter), {active: (current_chapter==chapter)})
        end

        concat paginate_chapter_page(paginate_chapter_content(book, total_pages, I18n.t("pagination.last")), {classes: "last", disabled: (current_chapter == total_pages)})
      end
    end
  end
  def paginate_chapter_page(content, options={})
    classes = [
      options[:classes],
      options[:disabled] ? "disabled" : nil,
      options[:active] ? "active" : nil
    ].compact

    content_tag :li, content, class: classes
  end
  def paginate_chapter_content(book, chapter, text=nil)
    text = text.html_safe if text
    text = text || chapter

    link_to text, chapter_path(book.to_chapter_path_hash(chapter))
  end

  # def chapters_slider(book, chapter, options={})
  #   options.reverse_merge!({
  #     min: 1,
  #     max: book.chapters_count,
  #     value: chapter
  #   })
  #
  #   content_tag :div, class: "row" do
  #     concat(content_tag(:div, "", class: "span6") do
  #       content_tag :div, "", class: "slider chapters-slider", data: {:"jquery-ui-slider-options" => options}
  #     end)
  #     concat(content_tag(:div, class: "span4") do
  #       I18n.t("words.sliders.n_of_m", n: content_tag(:span, options[:value], class: "chapters-slider-n"), m: options[:max]).html_safe
  #     end)
  #   end
  # end

  def follow_button(user)
    return "" unless user && logged_in?
    return "" if user == current_user

    if current_user.following?(user)
      link_to I18n.t("words.unfollow"), follow_user_path(user), method: :post, class: "btn danger small"
    else
      link_to I18n.t("words.follow"), follow_user_path(user), method: :post, class: "btn success small"
    end
  end

  def join_button(virtual_church)
    return "" unless virtual_church && logged_in?
    return "" if current_user.owned?(virtual_church)

    if current_user.joined?(virtual_church)
      link_to t("words.leave"), [:join, virtual_church], method: :post, class: "btn danger small"
    else
      link_to t("words.join"), [:join, virtual_church], method: :post, class: "btn success small"
    end
  end

  def available_locales_for_select(chosen_locale_code)
    chosen_locale_code ||= I18n.locale
    options_for_select(available_locales.collect { |locale| [locale.name, locale.locale_code]}, chosen_locale_code)
  end

  def embed_map(fields={})
    content_tag :iframe, "", src: map_path(fields), style: "width: 512px; height: 350px;"
  end

  def gmaps_on_same_page(obj)
    json = obj.to_gmaps4rails
    if @gmaps_count
      @gmaps_count += 1
      gmaps(markers: { data: json }, map_options: { id: "map#{@gmaps_count}", auto_adjust: true, auto_zoom: false, zoom: obj.zoom }, scripts: :none)
    else
      @gmaps_count = 1
      gmaps(markers: { data: json }, map_options: { auto_adjust: true, auto_zoom: false, zoom: obj.zoom })
    end
  end

  def polaroid(image_url, caption="", div_options={}, img_options={})
    div_options.reverse_merge!({
      class: ""
    })

    default_class = %w(polaroid)

    div_options[:class] = (div_options[:class].split(/ /) + default_class).uniq.join(" ")

    content_tag :figure, div_options do
      image_tag(image_url, img_options) +
      (caption.blank? ? "" : content_tag(:figcaption, caption))
    end
  end
end
