# encoding: utf-8

require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the HtmlTagsHelper. For example:
#
# describe HtmlTagsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       helper.concat_strings("this","that").should == "this that"
#     end
#   end
# end
describe HtmlTagsHelper, focus: true do
  describe "sign_in_out_link" do
    it "return logout link if logged in" do
      helper.stub!(:logged_in?).and_return(true)

      helper.sign_in_out_link.should match(I18n.t("words.signout"))
    end

    it "return login link if not logged in" do
      helper.stub!(:logged_in?).and_return(false)

      helper.sign_in_out_link.should match(I18n.t("words.signin"))
    end
  end

  describe "registration_profile_link" do
    it "return profile link if logged in" do
      identity = Factory.create(:identity)
      user = Factory.create(:user, uid: identity.id, email: identity.email)
      helper.stub!(:logged_in?).and_return(true)
      helper.stub!(:current_user).and_return(user)

      helper.registration_profile_link.should match(I18n.t("words.profile"))
    end

    it "return registration link if not logged in" do
      helper.stub!(:logged_in?).and_return(false)

      helper.registration_profile_link.should match(I18n.t("words.register"))
    end
  end

  describe "copyright" do
    it "contains the site name" do
      helper.copyright.should match(I18n.t("site.name"))
    end

    it "contins current year" do
      helper.copyright.should match(Time.now.year.to_s)
    end
  end

  describe "follow_button" do
    let(:identity) { Factory.create(:identity) }
    let(:user) { Factory.create(:user, uid: identity.id, email: identity.email) }

    context "returning empty string" do
      it "with nil parameter" do
        helper.follow_button(nil).should == ""
      end

      it "if not logged in" do
        helper.stub!(:logged_in?).and_return(false)

        helper.follow_button(user).should == ""
      end

      it "if following itself" do
        helper.stub!(:logged_in?).and_return(true)
        helper.stub!(:current_user).and_return(user)

        helper.follow_button(user).should == ""
      end
    end

    context "returning button" do
      let(:current_identity) { Factory.create(:identity) }
      let(:current_user) { Factory.create(:user, uid: current_identity.id, email: current_identity.email) }

      before(:each) do
        helper.stub!(:logged_in?).and_return(true)
        helper.stub!(:current_user).and_return(current_user)
      end

      it "for follow if not following yet" do
        helper.follow_button(user).should match(I18n.t("words.follow"))
      end

      it "for unfollow if following already" do
        current_user.follow!(user)

        helper.follow_button(user).should match(I18n.t("words.unfollow"))
      end
    end
  end

  describe "join_button" do
    let(:identity) { Factory.create(:identity) }
    let(:current_user) { Factory.create(:user, uid: identity.id, email: identity.email) }
    let(:virtual_church) { Factory.create(:virtual_church) }

    before(:each) do
      helper.stub!(:logged_in?).and_return(true)
      helper.stub!(:current_user).and_return(current_user)
    end

    it "return the join button" do
      helper.join_button(virtual_church).should match(I18n.t("words.join"))
    end

    it "return the leave button" do
      current_user.join!(virtual_church)

      helper.join_button(virtual_church).should match(I18n.t("words.leave"))
    end
  end

  describe "available_locales_for_select" do
    it "return select element" do
      helper.stub!(:available_locales).and_return(Locale.all)
      helper.available_locales_for_select(:en).should eq("<option value=\"en\" selected=\"selected\">English</option>\n<option value=\"zh-HK\">繁體中文</option>")
    end
  end

  describe "paginate_chapter_content" do
    it "return link to the chapter" do
      book = Book.instance_of(:kjv, :gen)

      helper.paginate_chapter_content(book, 1).should eq("<a href=\"/read/gen/1\">1</a>")
    end
  end

  describe "paginate_chapter_page" do
    it "return li containing the link to the chapter" do
      helper.paginate_chapter_page("content", {disabled: true}).should eq("<li class=\"disabled\">content</li>")
    end
  end

  describe "paginate_chapters" do
    it "return div containing the whole pagination" do
      book = Book.instance_of(:kjv, :gen)

      Factory.create(:verse, bible_code: :kjv, book_code: :gen, chapter: 1, verse: 1)

      helper.paginate_chapters(book, 3).should eq("<div class=\"pagination\"><ul><li class=\"first\"><a href=\"/read/gen/1\">&laquo; 最前</a></li><li class=\"\"><a href=\"/read/gen/1\">1</a></li><li class=\"last\"><a href=\"/read/gen/1\">最後 &raquo;</a></li></ul></div>")
    end
  end
end
