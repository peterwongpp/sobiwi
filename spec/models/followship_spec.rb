require 'spec_helper'

describe Followship, focus: true do
  it "should belongs to a user" do
    lambda { Followship.new.user }.should_not raise_error
  end
  it "should belongs to a following_user" do
    lambda { Followship.new.following_user }.should_not raise_error
  end
end
