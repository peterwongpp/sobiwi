require 'spec_helper'

describe Identity do
  let(:identity) { Factory.create(:identity) }
  
  context "on create" do
    let(:existing_identity) { Factory.create(:identity) }
    let(:identity) { Factory.build(:identity) }
    
    it "fails validation with no name" do
      identity.name = ""
      identity.should_not be_valid
      identity.should have(1).errors_on(:name)
    end
    
    it "passes validation with a name" do
      identity.name = "Peter"
      identity.should be_valid
      identity.should have(:no).errors_on(:name)
    end
    
    it "passes validation with a duplicated name" do
      identity.name = existing_identity.name
      identity.should be_valid
      identity.should have(:no).errors_on(:name)
    end
    
    it "fails validation with no email" do
      identity.email = ""
      identity.should_not be_valid
      identity.should have(1).error_on(:email)
    end
    
    it "fails validation with a duplicated email" do
      identity.email = existing_identity.email
      identity.should_not be_valid
      identity.should have(1).error_on(:email)
    end
    
    it "fails validation with an invalid email" do
      identity.email = "abc"
      identity.should_not be_valid
      identity.should have(1).error_on(:email)
    end
    
    it "passes validation with a unique email" do
      identity.email = "unique@email.com"
      identity.should be_valid
      identity.should have(:no).errors_on(:email)
    end
    
    it "fails validation with no password" do
      identity.password = ""
      identity.password_confirmation = ""
      identity.should_not be_valid
      identity.should have(1).error_on(:password)
    end
    
    it "fails validation with a too-short password" do
      identity.password = "12345" # it should be at least 6 characters long
      identity.password_confirmation = "12345"
      identity.should_not be_valid
      identity.should have(1).error_on(:password)
    end
    
    it "fails validation with a non-matched password_confirmation" do
      identity.password = "123456"
      identity.password_confirmation = "12345"
      identity.should_not be_valid
      identity.should have(1).error_on(:password)
    end
    
    it "fails validation with a too-short password and a non-matched password_confirmaion" do
      identity.password = "12345"
      identity.password_confirmation = "123456"
      identity.should_not be_valid
      identity.should have(2).errors_on(:password)
    end
  end
  
  context "on save" do
    let(:existing_identity) { Factory.create(:identity) }
    let(:identity) { Factory.create(:identity) }
    
    it "fails validation with no name" do
      identity.name = ""
      identity.should_not be_valid
      identity.should have(1).error_on(:name)
    end
    
    it "passes validation with a name" do
      identity.name = "Peter"
      identity.should be_valid
      identity.should have(:no).errors_on(:name)
    end
    
    it "passes validation with a duplicated name" do
      identity.name = existing_identity.name
      identity.should be_valid
      identity.should have(:no).errors_on(:name)
    end
    
    it "fails validation with no email" do
      identity.email = ""
      identity.should_not be_valid
      identity.should have(1).error_on(:email)
    end
    
    it "fails validation with a duplicated email" do
      identity.email = existing_identity.email
      identity.should_not be_valid
      identity.should have(1).error_on(:email)
    end
    
    it "fails validation with an invalid email" do
      identity.email = "abc"
      identity.should_not be_valid
      identity.should have(1).error_on(:email)
    end
    
    it "passes validation with a unique email" do
      identity.email = "unique@email.com"
      identity.should be_valid
      identity.should have(:no).errors_on(:email)
    end
    
    it "passes validation with no password and password_confirmaion given" do
      identity.password = nil
      identity.password_confirmation = nil
      identity.should be_valid
      identity.should have(:no).errors_on(:password)
    end
    
    it "fails validation with a blank password is given" do
      identity.password = ""
      identity.password_confirmation = ""
      identity.should_not be_valid
      identity.should have(1).error_on(:password)
    end
    
    it "fails validation with a too-short password" do
      identity.password = "12345" # it should be at least 6 characters long
      identity.password_confirmation = "12345"
      identity.should_not be_valid
      identity.should have(1).error_on(:password)
    end
    
    it "fails validation with a non-matched password_confirmation" do
      identity.password = "123456"
      identity.password_confirmation = "12345"
      identity.should_not be_valid
      identity.should have(1).error_on(:password)
    end
    
    it "fails validation with a too-short password and a non-matched password_confirmaion" do
      identity.password = "12345"
      identity.password_confirmation = "123456"
      identity.should_not be_valid
      identity.should have(2).errors_on(:password)
    end
  end
  
  it "generates and stores the forget password token" do
    identity.forget_password_token.should be_blank
    identity.forget_password_token_created_at.should be_blank
    
    identity.generate_forget_password_token!
    
    identity.forget_password_token.should_not be_blank
    identity.forget_password_token_created_at.should_not be_blank
  end
  
  it "checks if the forget password token expired or not" do
    identity.should_not be_forget_password_expired
    
    identity.forget_password_token_created_at = 2.days.ago
    identity.should be_forget_password_expired
    
    identity.forget_password_token_created_at = (2.days.ago+1.second)
    identity.should_not be_forget_password_expired
  end
  
  it "deactivates the forget password token" do
    identity.generate_forget_password_token!
    
    identity.deactivate_forget_password_token!
    
    identity.forget_password_token.should be_blank
    identity.forget_password_token_created_at.should be_blank
  end
end
