require 'spec_helper'

describe User do
  let(:identity) { Factory.create(:identity) }
  let(:user) { Factory.create(:user, provider: "identity", uid: identity.id) }
  
  context "#from_omniauth" do
    it "returns a user if the user exists already" do
      user = Factory.create(:user, provider: "identity", uid: identity.id)
      
      the_user = User.from_omniauth({
        "provider" => "identity",
        "uid" => identity.id
      })
      
      the_user.should == user
    end
    
    it "creates and returnes a user if the user does not exist" do
      user = User.from_omniauth({
        "provider" => "identity",
        "uid" => 1314,
        "info" => {
          "name" => "something",
          "email" => ""
        }
      })
      
      user.should be_persisted
    end
    
    context "#create_with_omniauth" do
      it "should create a user based on the auth hash returned from omniauth" do
        user = User.create_with_omniauth({
          "provider" => "identity",
          "uid" => 1314,
          "info" => {
            "name" => "something",
            "email" => ""
          }
        })
      
        user.should be_persisted
      end
    end
  end
  
  context "follow" do
    let(:user1) { Factory.create(:user) }
    let(:user2) { Factory.create(:user) }
    
    it "should follow the user" do
      user1.follow!(user2)
      
      user1.should be_following(user2)
    end
    
    it "should not be able to follow itself" do
      user1.follow!(user1)
      
      user1.should_not be_following(user1)
    end
    
    it "should unfollow the user if calling the follow! method again" do
      user1.follow!(user2)
      user1.should be_following(user2)
      user1.follow!(user2)
      
      user1.should_not be_following(user2)
    end
  end
  
  context "owned virtual churches" do
    let(:user) { Factory.create(:user) }
    let(:virtual_church) { Factory.create(:virtual_church) }
    
    it "return true if the virtual church is created by the user" do
      virtual_church.creator = user
      user.owned?(virtual_church).should == true
    end
    
    it "return false if the virtual church is not created by the user" do
      user.owned?(virtual_church).should == false
    end
  end
  
  context "joining virtual churches" do
    let(:user) { Factory.create(:user) }
    let(:virtual_church) { Factory.create(:virtual_church) }
    
    it "should join the virtual church" do
      user.join!(virtual_church)
      
      user.should be_joined(virtual_church)
    end
    
    it "should not be able to join its owned virtual churches" do
      owned_virtual_church = Factory.create(:virtual_church, creator: user)
      
      user.join!(owned_virtual_church)
      
      user.should_not be_joined(owned_virtual_church)
    end
    
    it "should leave the virtual church if calling the join! method again" do
      user.join!(virtual_church)
      user.join!(virtual_church)
      
      user.should_not be_joined(virtual_church)
    end
  end
end
