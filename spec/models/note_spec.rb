require 'spec_helper'

describe Note, focus: true do
  it "returns notes under a verse" do
    @verse = Factory.create(:verse)
    @note = Factory.create(:note)
    Factory.create(:annotation, note: @note, verse: @verse)
    @note2 = Factory.create(:note)

    Note.under(@verse).should eq([@note])
  end
  it "returns notes under verses" do
    @verse1 = Factory.create(:verse)
    @verse2 = Factory.create(:verse)
    @note1 = Factory.create(:note)
    Factory.create(:annotation, note: @note1, verse: @verse1)
    @note2 = Factory.create(:note)
    Factory.create(:annotation, note: @note2, verse: @verse2)
    @note3 = Factory.create(:note)

    Note.under([@verse1, @verse2]).should eq([@note1, @note2])
  end
end
