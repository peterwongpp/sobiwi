require 'spec_helper'

describe Membership, focus: true do
  it "should belongs to a user" do
    lambda { Membership.new.user }.should_not raise_error
  end
  it "should belongs to a virtual_church" do
    lambda { Membership.new.virtual_church }.should_not raise_error
  end
end
