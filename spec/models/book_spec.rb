# encoding: UTF-8

require 'spec_helper'

describe Book do
  let(:book) { Book.instance_of(:kjv, :gen) }
  
  context "#instance_of" do
    it "return a book in old testament according to the bible_code and the book_code" do
      book = Book.instance_of(:kjv, :gen)
      
      book.should_not be_nil
      book.bible_code.should == :kjv
      book.testament_code.should == :old
      book.book_code.should == :gen
    end
    
    it "return a book in new testament according to the bible_code and the book_code" do
      book = Book.instance_of(:kjv, :matt)
      
      book.should_not be_nil
      book.bible_code.should == :kjv
      book.testament_code.should == :new
      book.book_code.should == :matt
    end
  end
  
  it "return a book's name" do
    book.name.should == I18n.t("bibles.#{book.bible_code}.testaments.#{book.testament_code}.books.#{book.book_code}.name")
  end
  
  it "return a book's short name" do
    book.short_name.should == I18n.t("bibles.#{book.bible_code}.testaments.#{book.testament_code}.books.#{book.book_code}.short_name")
  end
  
  it "return a book's bible" do
    book.bible.should_not be_nil
    book.bible.bible_code.should == :kjv
  end
  
  it "return a book's testament" do
    book.testament.should_not be_nil
    book.testament.testament_code.should == :old
  end
  
  it "return chapters count" do
    10.times do |i|
      Factory.create(:verse, chapter: (i+1))
    end
    book.chapters_count.should == 10
  end
  
  it "return verses in a given chapter" do
    verse = Factory.create(:verse)
    Factory.create(:verse)
    Factory.create(:verse)
    book = verse.book
    
    verses = book.verses_in_chapter(verse.chapter)
    
    verses.should have(3).items
  end
  
  context "#to_chapter_path_hash" do
    it "return a hash for using in chapter_path" do
      book.to_chapter_path_hash(3).should == {
        book_code: book.book_code,
        chapter: 3
      }
    end
  end
  
  context "#to_verse_path_hash" do
    it "return a hash for using in verse_path" do
      book.to_verse_path_hash(3, 2).should == {
        book_code: book.book_code,
        chapter: 3,
        verse: 2
      }
    end
  end
end