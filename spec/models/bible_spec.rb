require 'spec_helper'

describe Bible do
  it "return all bibles" do
    Bible.all.count.should == 2
    Bible.all.first.bible_code.should == :kjv
    Bible.all.last.bible_code.should == :unv
  end
  
  context "#has?" do
    it "return true if the bible_code exists" do
      Bible.has?("unv").should == true
      Bible.has?(:kjv).should == true
    end
    
    it "return false if the bible_code does not exist" do
      Bible.has?("not-exist").should == false
    end
  end
  
  it "return a bible according to the bible's code" do
    bible = Bible.instance_of(:kjv)
    bible.should_not be_nil
    bible.bible_code.should == :kjv
  end
  
  it "return a bible's name" do
    Bible.all.each do |bible|
      bible.name.should == I18n.t("bibles.#{bible.bible_code}.name")
    end
  end
  
  it "return a bible's short name" do
    Bible.all.each do |bible|
      bible.short_name.should == I18n.t("bibles.#{bible.bible_code}.short_name")
    end
  end
  
  it "return a bible's testaments" do
    Bible.all.each do |bible|
      bible.testaments.count.should == 2
      bible.testaments.each { |testament| testament.should be_kind_of(Testament) }
    end
  end
end