require 'spec_helper'

describe MessageThreadUser, focus: true do
  it "should belongs to a user" do
    lambda { MessageThreadUser.new.user }.should_not raise_error
  end
  it "should belongs to a message_thread" do
    lambda { MessageThreadUser.new.message_thread }.should_not raise_error
  end
end
