require 'spec_helper'

describe Message, focus: true do
  it "should return the latest n messages" do
    @message1 = Factory.create(:message, created_at: Time.now)
    @message2 = Factory.create(:message, created_at: 1.days.ago)
    @message3 = Factory.create(:message, created_at: 1.year.ago)

    messages = Message.latest(2)

    messages.should eq([@message1, @message2])
  end
end
