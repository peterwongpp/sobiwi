require 'spec_helper'

describe MessageThread, focus: true do
  it "generate uniq_id after create" do
    @iden1 = Factory.create(:identity)
    @user1 = Factory.create(:user, uid: @iden1.id, email: @iden1.email)
    @iden2 = Factory.create(:identity)
    @user2 = Factory.create(:user, uid: @iden2.id, email: @iden2.email)

    @mt = Factory.build(:message_thread)
    @mt.message_thread_users.build(user: @user1)
    @mt.message_thread_users.build(user: @user2)
    @mt.save!

    @mt.uniq_id.should == [@user1.id, @user2.id].sort.join("--")
  end

  it "returns message threads involving a user" do
    @iden = Factory.create(:identity)
    @user = Factory.create(:user, uid: @iden.id, email: @iden.email)
    @mt1 = Factory.build(:message_thread)
    @mt1.message_thread_users.build(user: @user)
    @mt1.save!
    @mt2 = Factory.build(:message_thread)

    MessageThread.involving(@user).should eq([@mt1])
  end

  it "should create the message thread if not yet exist" do
    @iden = Factory.create(:identity)
    @user = Factory.create(:user, uid: @iden.id, email: @iden.email)

    message_thread = MessageThread.reuse_or_create(@user, {user_ids: [], messages: { content: "yo" } })

    message_thread.should be_persisted
  end
  it "should reuse the message thread if existed" do
    @iden = Factory.create(:identity)
    @user = Factory.create(:user, uid: @iden.id, email: @iden.email)
    @mt = Factory.build(:message_thread)
    @mt.message_thread_users.build(user: @user)
    @mt.save!

    message_thread = MessageThread.reuse_or_create(@user, {user_ids: [], messages: { content: "yo" } })

    message_thread.should == @mt
  end
end
