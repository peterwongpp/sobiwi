require 'spec_helper'

describe Verse, focus: true do
  let(:verse) { Factory.create(:verse) }

  it "should return the testament code" do
    verse.testament.testament_code.should == :old
  end

  it "should return bible" do
    verse.bible.should be_kind_of(Bible)
  end

  it "should return testament" do
    verse.testament.should be_kind_of(Testament)
  end

  it "should return book" do
    verse.book.should be_kind_of(Book)
  end

  it "return the same verse in another bible verse" do
    verse2 = Factory.create(:verse, bible_code: "unv", verse: verse.verse)

    verse.in_bible_version(:unv).should be_kind_of(Verse)
  end

  it "should return a name base on its book and chapter" do
    verse.name.should == "#{verse.book.short_name} #{verse.chapter}:#{verse.verse}"
  end

  context "#to_verse_path_hash" do
    it "return a hash for using in verse_path" do
      verse.to_verse_path_hash.should == {
        book_code: verse.book_code,
        chapter: verse.chapter,
        verse: verse.verse
      }
    end
  end

  # let(:verse) { Factory.create(:verse) }
  #
  # it "should get all verses given a bible, a book and a chapter" do
  #   # given
  #   v_same2 = Factory.create(:verse)
  #   v_samw3 = Factory.create(:verse)
  #   v_diff1 = Factory.create(:verse, chapter: 2)
  #
  #   # when
  #   verses = Verse.where(bible_code: verse.bible_code, book_code: verse.book_code, chapter: verse.chapter)
  #
  #   # then
  #   verses.count.should == 3
  # end
  #
  # it "should return the corresponding bible object" do
  #   bible = Bible.new(verse.bible_code)
  #
  #   verse.bible.code.should == bible.code
  # end
  #
  # it "should return the corresponding book object" do
  #   book = Book.new(verse.bible_code, verse.book_code)
  #
  #   verse.book.code.should == book.code
  #   verse.bible.code.should == book.bible_code
  # end
end