require 'spec_helper'

describe Annotation, focus: true do
  it "should belongs to a note" do
    lambda { Annotation.new.note }.should_not raise_error
  end
  it "should belongs to a verse" do
    lambda { Annotation.new.verse }.should_not raise_error
  end
end
