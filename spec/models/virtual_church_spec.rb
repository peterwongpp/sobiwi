require 'spec_helper'

describe VirtualChurch, focus: true do
  it "should belongs to a creator" do
    lambda { VirtualChurch.new.creator }.should_not raise_error
  end
  it "should have many memberships" do
    lambda { VirtualChurch.new.memberships }.should_not raise_error
  end
end
