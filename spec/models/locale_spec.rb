require 'spec_helper'

describe Locale, focus: true do
  it "return all locales" do
    Locale.all.count.should == 2
  end

  context "#has?" do
    it "return true if the locale exists" do
      Locale.has?("zh-HK").should == true
      Locale.has?(:en).should == true
    end

    it "return false if the locale does not exist" do
      Locale.has?("not-exist").should == false
    end
  end

  context "#instance_of" do
    it "return a locale object according to the locale code" do
      locale = Locale.instance_of(:en)

      locale.should_not be_nil
      locale.locale_code.should == :en
      locale.name.should == I18n.t("locales.#{locale.locale_code}")
    end
  end
end