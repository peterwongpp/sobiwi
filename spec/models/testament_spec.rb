require 'spec_helper'

describe Testament do
  let(:testament) { Testament.instance_of(:kjv, :old) }
  
  context "#instance_of" do
    it "return a old_testament according to the bible's code and the testament's code" do
      testament = Testament.instance_of(:kjv, :old)
      
      testament.should_not be_nil
      testament.bible_code.should == :kjv
      testament.testament_code.should == :old
    end
    
    it "return a new_testament according to the bible's conde and the testament's code" do
      testament = Testament.instance_of(:kjv, :new)
      
      testament.should_not be_nil
      testament.bible_code.should == :kjv
      testament.testament_code.should == :new
    end
  end
  
  it "return a testament's name" do
    testament.name.should == I18n.t("bibles.#{testament.bible_code}.testaments.#{testament.testament_code}.name")
  end
  
  it "return a testament's bible" do
    testament.bible.should_not be_nil
    testament.bible.bible_code.should == :kjv
  end
  
  it "return a testament's books" do
    testament.books.count.should == 39
  end
end