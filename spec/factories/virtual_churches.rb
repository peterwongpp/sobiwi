# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :virtual_church do
    sequence(:name) { |n| "vc#{n}"}
    description     "This is a description of the church."
  end
end
