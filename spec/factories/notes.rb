# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :note do
    label_color "#edc"

    factory :note_share, class: NoteShare do
      content "the content of the share"
    end

    factory :note_note, class: NoteNote do
      title   "the title of the note"
      content "the content of the note"
    end
  end
end
