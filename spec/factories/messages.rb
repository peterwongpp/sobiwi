# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :message do
    message_thread nil
    user nil
    sequence(:content) { |n| "Message #{n}" }
  end
end
