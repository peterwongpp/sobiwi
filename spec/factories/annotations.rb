# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :annotation do
    association :note, method: :build
    association :verse, method: :build
    from_atom_index 0
    to_atom_index   3
  end
end
