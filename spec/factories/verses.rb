# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :verse do
    bible_code "kjv"
    book_code "gen"
    chapter 1
    sequence(:verse)    { |n| n }
    sequence(:content)  { |n| "content #{n}"}
  end
end
