# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :followship do
    association :user, method: :build
    association :following_user, factory: :user, method: :build
  end
end
