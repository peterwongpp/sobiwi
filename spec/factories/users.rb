# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    provider                "identity"
    sequence(:uid)          { |n| n }
    sequence(:display_name) { |n| "U#{n}"}
    sequence(:email)        { |n| "u#{n}@users.com" }
    chosen_locale_code      "zh-HK"
    chosen_bible_code       "unv"
  end
end
