# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :identity do
    sequence(:name)   { |n| "identity_#{n}" }
    sequence(:email)  { |n| "u#{n}@users.com" }
    password          "123123"
    password_confirmation "123123"
  end
end
