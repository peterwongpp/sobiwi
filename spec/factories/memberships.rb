# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :membership do
    association :user, method: :build
    association :virtual_church, method: :build
  end
end
