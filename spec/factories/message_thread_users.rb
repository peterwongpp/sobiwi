# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :message_thread_user do
    message_thread nil
    user nil
  end
end
