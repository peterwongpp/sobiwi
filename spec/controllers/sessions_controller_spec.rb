require 'spec_helper'

describe SessionsController do
  describe "delete destroy" do
    it "log the current user out" do
      session[:user_id] = 1
    
      delete :destroy
    
      session[:user_id].should be_nil
    end
    
    it "set the correct flash message" do
      delete :destroy
      
      flash[:default][0][:message_type].should == :success
    end
    
    it "should redirect to root" do
      delete :destroy
      
      response.should redirect_to(root_path)
    end
  end
  
  describe "get failure" do
    it "push flash message" do
      get :failure
      
      flash[:login_form][0][:message_type].should == :failure
    end
    
    it "redirect to login path" do
      get :failure
      
      response.should redirect_to(login_path)
    end
  end
  
  describe "post create" do
    before(:each) do
      controller.stub!(:env).and_return({
        "omniauth.auth" => {
          "provider" => "identity",
          "uid" => "1",
          "info" => {
            "name" => "peter",
            "email" => ""
          }
        }
      })
      
      session[:chosen_locale_code] = :en
      session[:chosen_bible_code] = :kjv
    end
    after(:each) do
      session[:chosen_locale_code] = nil
      session[:chosen_bible_code] = nil
    end
    
    context "when succeeded" do
      before(:each) do
        post :create, provider: controller.env["omniauth.auth"]["provider"]
      end
      
      it "create the user if the user not exist" do
        assigns(:user).provider.should == "identity"
        assigns(:user).uid.should == "1"
      end
      
      it "update the user's chosen_locale_code and chosen_bible_code" do
        assigns(:user).chosen_locale_code.should == :en
        assigns(:user).chosen_bible_code.should == :kjv
      end
      
      it "set flash message" do
        flash[:default][0][:message_type].should == :success
      end
    end
  end
end
