require 'spec_helper'

describe VirtualChurchesController do
  let(:user) { Factory.create(:user) }
  let(:virtual_church) { Factory.create(:virtual_church, creator: user) }
    
  before(:each) do
    session[:user_id] = user.id
  end
    
  after(:each) do
    session[:user_id] = nil
  end
  
  describe "put update" do
    it "updates successfully" do
      put :update, id: virtual_church.id, virtual_church: { name: "new name" }
      
      response.should redirect_to(virtual_church_path(virtual_church))
    end
    
    it "updates failed due to validation" do
      existing_virtual_church = Factory.create(:virtual_church)
      
      put :update, id: virtual_church.id, virtual_church: { name: existing_virtual_church.name }
      
      response.should redirect_to(edit_virtual_church_path(virtual_church))
    end
    
    it "updates failed due to not logged in" do
      session[:user_id] = nil
      
      put :update, id: virtual_church.id, virtual_church: {}
      
      response.should redirect_to(root_path)
    end
  end
  
  describe "delete destroy" do
    it "destroy the virtual church" do
      delete :destroy, id: virtual_church.id
      
      assigns(:virtual_church).should_not be_persisted
    end
    
    it "should not destory if the current user is not the creator" do
      creator = Factory.create(:user)
      virtual_church.creator = creator
      virtual_church.save
      
      delete :destroy, id: virtual_church.id
      
      assigns(:virtual_church).should be_persisted
    end
  end
  
  describe "post join" do
    it "joins the virtual church" do
      creator = Factory.create(:user)
      virtual_church.creator = creator
      virtual_church.save
      
      request.env["HTTP_REFERER"] = "/"
      
      post :join, id: virtual_church.id
      
      controller.current_user.should be_joined(virtual_church)
    end
  end
end
