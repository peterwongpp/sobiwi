require 'spec_helper'

describe MessageThreadsController, focus: true do
  before(:each) do
    @identity = Factory.create(:identity)
    @user = Factory.create(:user, uid: @identity.id, email: @identity.email)
    session[:user_id] = @user.id
  end

  describe "GET index" do
    it "requires logged in" do
      session[:user_id] = nil
      get :index
      response.should redirect_to(root_path)
    end
    it "assigns @message_threads" do
      shown_mt = Factory.create(:message_thread)
      Factory.create(:message_thread_user, message_thread: shown_mt, user: @user)
      Factory.create(:message, message_thread: shown_mt, user: @user, content: "message")

      not_shown_mt1 = Factory.create(:message_thread)
      # not involving
      Factory.create(:message, message_thread: shown_mt, content: "message")

      not_shown_mt2 = Factory.create(:message_thread)
      Factory.create(:message_thread_user, message_thread: shown_mt, user: @user)
      # no message

      get :index
      assigns(:message_threads).should eq([shown_mt])
    end
    it "renders the index template" do
      get :index
      response.should render_template("index")
    end
  end

  describe "GET show" do
    before(:each) do
      @identity2 = Factory.create(:identity)
      @user2 = Factory.create(:user, uid: @identity2.id, email: @identity2.email)
      @message_thread = Factory.create(:message_thread)
      @message_thread_user = Factory.create(:message_thread_user, user: @user, message_thread: @message_thread)
      @message_thread_user2 = Factory.create(:message_thread_user, user: @user2, message_thread: @message_thread)
      @messages = []
      11.times { |n| @messages << Factory.create(:message, message_thread: @message_thread, user: @user, content: "message #{n}") }
    end

    it "requires logged in" do
      session[:user_id] = nil
      get :show
      response.should redirect_to(root_path)
    end
    it "assigns @message_thread" do
      get :show, id: @message_thread.id
      assigns(:message_thread).should == @message_thread
    end
    it "assigns @users" do
      get :show, id: @message_thread.id
      assigns(:users).should eq([@user2])
    end
    it "assigns paginated @messages" do
      get :show, id: @message_thread.id
      assigns(:messages).should eq(@messages[0, 10])
    end
    it "renders the show template" do
      get :show, id: @message_thread.id
      response.should render_template("show")
    end
  end

  describe "GET new" do
    it "requires logged in" do
      session[:user_id] = nil
      get :new
      response.should redirect_to(root_path)
    end
    it "assigns @message_thread" do
      get :new
      assigns(:message_thread).should_not be_nil
    end
    it "assigns @following_friends" do
      @identity2 = Factory.create(:identity)
      @user2 = Factory.create(:user, uid: @identity2.id, email: @identity2.email)
      @user.follow!(@user2)

      get :new
      assigns(:following_friends).should eq([@user2])
    end
    it "assigns @friends_following_me" do
      @identity2 = Factory.create(:identity)
      @user2 = Factory.create(:user, uid: @identity2.id, email: @identity2.email)
      @user2.follow!(@user)

      get :new
      assigns(:friends_following_me).should eq([@user2])
    end
    it "renders the new template" do
      get :new
      response.should render_template("new")
    end
  end

  describe "POST create" do
    it "requires logged in" do
      session[:user_id] = nil
      post :create
      response.should redirect_to(root_path)
    end
    it "assigns @message_thread" do
      post :create, message_thread: { user_ids: [], messages: { content: "yo" } }
      assigns(:message_thread).should_not be_nil
    end
    it "redirects to message thread" do
      post :create, message_thread: { user_ids: [], messages: { content: "yo" } }
      response.should redirect_to(message_thread_path(assigns(:message_thread)))
    end
  end
end
