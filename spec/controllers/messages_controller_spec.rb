require 'spec_helper'

describe MessagesController, focus: true do
  before(:each) do
    @identity = Factory.create(:identity)
    @user = Factory.create(:user, uid: @identity.id, email: @identity.email)
    session[:user_id] = @user.id

    @message_thread = Factory.create(:message_thread)
    @message_thread_user = Factory.create(:message_thread_user, message_thread: @message_thread, user: @user)
  end

  describe "POST create" do
    it "requires logged in" do
      session[:user_id] = nil
      post :create
      response.should redirect_to(root_path)
    end
    it "assigns @message_thread" do
      post :create, message_thread_id: @message_thread.id
      assigns(:message_thread).should == @message_thread
    end
    it "assigns @message" do
      post :create, message_thread_id: @message_thread.id, message: { content: "this is a message." }
      assigns(:message).content.should == "this is a message."
    end
  end

  describe "DELETE destroy" do
    it "requires logged in" do
      session[:user_id] = nil
      delete :destroy
      response.should redirect_to(root_path)
    end
    it "assigns @message_thread" do
      post :create, message_thread_id: @message_thread.id
      assigns(:message_thread).should == @message_thread
    end
    it "assigns @message" do
      @message = Factory.create(:message, user: @user, message_thread: @message_thread, content: "message again")
      post :create, message_thread_id: @message_thread.id, id: @message.id
      assigns(:message).should_not be_persisted
    end
  end
end
