require 'spec_helper'

describe ApplicationController do
  controller do
    def index
      @available_locales = available_locales
      
      @chosen_locale = chosen_locale
      
      render text: ""
    end
  end
  
  describe "#available_locales" do
    it "returns all locales" do
      get :index
      
      assigns(:available_locales).should == Locale.all
    end
  end
  
  describe "#chosen_locale" do
    it "return the chosen locale" do
      session[:chosen_locale_code] = :en
      
      get :index
      
      assigns(:chosen_locale).should == Locale.instance_of(:en)
    end
  end
  
  describe "#choose_locale" do
    before(:each) do
      @old_locale = :en
      @new_locale = :"zh-HK"
      
      session[:chosen_locale_code] = @old_locale
    end
    after(:each) do
      session[:chosen_locale_code] = @old_locale
    end
    
    context "when logged in" do
      before(:each) do
        user = Factory.create(:user)
        session[:user_id] = user.id
      end
      after(:each) do
        user = nil
        session[:user_id] = nil
      end
      
      context "and has no HTTP_REFERER set" do
        it "store the chosen locale to the user" do
          get :index, locale: @new_locale
          
          controller.current_user.chosen_locale_code.should == @new_locale
        end
        
        it "set the session of chosen_locale_code" do
          get :index, locale: @new_locale
          
          session[:chosen_locale_code].should == @new_locale
        end
      end
    end
  end
  
  describe "#choose_bible" do
    before(:each) do
      @old_bible = :kjv
      @new_bible = :unv
      
      session[:chosen_bible_code] = @old_bible
    end
    after(:each) do
      session[:chosen_bible_code] = @old_bible
    end
    
    context "when logged in" do
      before(:each) do
        user = Factory.create(:user)
        session[:user_id] = user.id
      end
      after(:each) do
        user = nil
        session[:user_id] = nil
      end
      
      context "and has no HTTP_REFERER set" do
        it "stores the chosen bible to the user" do
          get :index, choose_bible: @new_bible
          
          controller.current_user.chosen_bible_code.should == @new_bible
        end
        
        it "sets the session of chosen_bible_code" do
          get :index, choose_bible: @new_bible
          
          session[:chosen_bible_code].should == @new_bible
        end
      end
    end
  end
end