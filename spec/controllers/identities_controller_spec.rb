require 'spec_helper'

describe IdentitiesController do
  describe "get new" do
    it "requires not logged in" do
      user = Factory.create(:user)
      session[:user_id] = user

      get :new

      response.should redirect_to(root_path)
    end

    it "generates the default registration form" do
      get :new

      assigns(:identity).should_not be_nil
    end

    it "submitted to omniauth but has errors" do
      identity = Factory.build(:identity, email: "")
      controller.stub!(:env).and_return({'omniauth.identity' => identity})

      get :new

      assigns(:identity).should == identity
      session[:tmp_new_identity].should_not be_nil
    end

    it "has tmp_new_identity already" do
      identity = Factory.build(:identity, email: "", password: "123", password_confirmation: "456")

      session[:tmp_new_identity] = {
        name: identity.name,
        email: identity.email,
        password: identity.password,
        password_confirmation: identity.password_confirmation
      }

      get :new

      assigns(:identity).should_not be_nil
      flash[:registration_form][0][:message_type].should == :failure
    end
  end
end
