require 'spec_helper'

describe NotesController, focus: true do
  before(:each) do
    @identity = Factory.create(:identity)
    @user = Factory.create(:user, uid: @identity.id, email: @identity.email)
    session[:user_id] = @user.id
  end

  describe "POST create" do
    it "requires logged in" do
      session[:user_id] = nil
      post :create
      response.should redirect_to(root_path)
    end
    it "assigns @note" do
      post :create, modal_chosen: "share",
                    bible_code: "unv", book_code: "gen", chapter: "1", from_verse: "1", from_verse_atom: "1", to_verse: "1", to_verse_atom: "3",
                    note_share: { content: "this is my note content" },
                    note: { privacy: "private", label_color: "yellow" }
      assigns(:note).should_not be_nil
    end
    it "redirects to chapter page" do
      post :create, modal_chosen: "share",
                    bible_code: "unv", book_code: "gen", chapter: "1", from_verse: "1", from_verse_atom: "1", to_verse: "1", to_verse_atom: "3",
                    note_share: { content: "this is my note content" },
                    note: { privacy: "private", label_color: "yellow" }
      response.should redirect_to(chapter_path(book_code: "gen", chapter: "1"))
    end
  end
end
