require 'spec_helper'

describe Users::VirtualChurchesController, focus: true do
  before(:each) do
    @identity = Factory.create(:identity)
    @user = Factory.create(:user, uid: @identity.id, email: @identity.email)
    session[:user_id] = @user.id
  end

  describe "GET index" do
    it "requires logged in" do
      session[:user_id] = nil
      get :index
      response.should redirect_to(root_path)
    end
    it "assigns @owned_virtual_churches" do
      vc = Factory.create(:virtual_church, creator: @user)
      get :index
      assigns(:owned_virtual_churches).should eq([vc])
    end
    it "assigns @memberships" do
      identity2 = Factory.create(:identity)
      user2 = Factory.create(:user, uid: identity2.id, email: identity2.email)
      vc = Factory.create(:virtual_church, creator: user2)
      membership = Factory.create(:membership, user: @user, virtual_church: vc)
      get :index
      assigns(:memberships).should eq([membership])
    end
    it "renders the index template" do
      get :index
      response.should render_template("index")
    end
  end

  describe "GET new" do
    it "requires logged in" do
      session[:user_id] = nil
      get :new
      response.should redirect_to(root_path)
    end
    it "assigns @virtual_church" do
      get :new
      assigns(:virtual_church).should_not be_nil
    end
    it "renders the new template" do
      get :new
      response.should render_template("new")
    end
    it "should get back the create-failed virtual church" do
      vc = Factory.build(:virtual_church, name: "", creator: @user)
      session[:virtual_church_create_update] = vc
      vc.should_not be_valid
      get :new
      assigns(:virtual_church).should == vc
    end
  end

  describe "POST create" do
    it "requires logged in" do
      session[:user_id] = nil
      post :create
      response.should redirect_to(root_path)
    end
    it "assigns @virtual_church on success" do
      post :create, virtual_church: { name: "New Church", description: "the description" }
      assigns(:virtual_church).should be_valid
      assigns(:virtual_church).name.should == "New Church"
      assigns(:virtual_church).description.should == "the description"
    end
    it "redirects to the virtual churchs page on success" do
      post :create, virtual_church: { name: "New Church", description: "the description" }
      response.should redirect_to(user_virtual_churches_path)
    end
    it "assigns @virtual_church on fail" do
      post :create, virtual_church: { name: "", description: "the description" }
      assigns(:virtual_church).should_not be_valid
      assigns(:virtual_church).name.should == ""
      assigns(:virtual_church).description.should == "the description"
      session[:virtual_church_create_update].name.should == ""
      session[:virtual_church_create_update].description.should == "the description"
    end
    it "redirects to the virtual churchs page on fail" do
      post :create, virtual_church: { name: "", description: "the description" }
      response.should redirect_to(new_user_virtual_church_path)
    end
  end

  describe "GET edit" do
    it "requires logged in" do
      session[:user_id] = nil
      get :edit
      response.should redirect_to(root_path)
    end
    it "assigns @virtual_church" do
      vc = Factory.create(:virtual_church, creator: @user)
      get :edit, id: vc.id
      assigns(:virtual_church).should == vc
    end
    it "renders the edit template" do
      vc = Factory.create(:virtual_church, creator: @user)
      get :edit, id: vc.id
      response.should render_template("edit")
    end
    it "should get back the update-failed virtual church" do
      vc = Factory.create(:virtual_church, creator: @user)
      vc.name = ""
      get :edit, id: vc.id
      session[:virtual_church_create_update] = vc
      vc.should_not be_valid
    end
  end

  describe "PUT update" do
    it "requires logged in" do
      session[:user_id] = nil
      put :update
      response.should redirect_to(root_path)
    end
    it "assigns @virtual_church on success" do
      vc = Factory.create(:virtual_church, creator: @user)
      put :update, id: vc.id, virtual_church: { name: "New Church", description: "the description" }
      assigns(:virtual_church).should be_valid
      assigns(:virtual_church).name.should == "New Church"
      assigns(:virtual_church).description.should == "the description"
    end
    it "redirects to the virtual churchs page on success" do
      vc = Factory.create(:virtual_church, creator: @user)
      put :update, id: vc.id, virtual_church: { name: "New Church", description: "the description" }
      response.should redirect_to(user_virtual_churches_path)
    end
    it "assigns @virtual_church on fail" do
      vc = Factory.create(:virtual_church, creator: @user)
      put :update, id: vc.id, virtual_church: { name: "", description: "the description" }
      assigns(:virtual_church).should_not be_valid
      assigns(:virtual_church).name.should == ""
      assigns(:virtual_church).description.should == "the description"
      session[:virtual_church_create_update].name.should == ""
      session[:virtual_church_create_update].description.should == "the description"
    end
    it "redirects to the virtual churchs page on fail" do
      vc = Factory.create(:virtual_church, creator: @user)
      put :update, id: vc.id, virtual_church: { name: "", description: "the description" }
      response.should redirect_to(edit_user_virtual_church_path(vc))
    end
  end

  describe "DELETE destroy" do
    it "requires logged in" do
      session[:user_id] = nil
      delete :destroy
      response.should redirect_to(root_path)
    end
    it "assigns @virtual_church" do
      vc = Factory.create(:virtual_church, creator: @user)
      delete :destroy, id: vc.id
      assigns(:virtual_church).should == vc
    end
    it "deletes the memberships" do
      vc = Factory.create(:virtual_church, creator: @user)
      @identity2 = Factory.create(:identity)
      @user2 = Factory.create(:user, uid: @identity2.id, email: @identity2.email)
      Factory.create(:membership, user: @user2, virtual_church: vc)
      delete :destroy, id: vc.id
      assigns(:virtual_church).should_not be_persisted
      assigns(:virtual_church).memberships.each do |membership|
        membership.should_not be_persisted
      end
    end
  end
end
