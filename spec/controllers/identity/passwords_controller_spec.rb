require 'spec_helper'

describe Identity::PasswordsController do
  let(:identity) { Factory.create(:identity) }
  
  describe "post create" do
    it "set the forget password token" do
      post :create, email: identity.email
      
      assigns(:identity).forget_password_token.should_not be_blank
    end
    
    it "redirect back and show error message if email is incorrect" do
      post :create, email: "not exist"
      
      flash[:reset_password_form].should_not be_blank
    end
  end
  
  describe "get edit" do
    it "redirect back to #new if no identity is given by the forget password token" do
      get :edit, id: "not exist"
      
      flash[:reset_password_form][0][:message_type].should == :token_not_exist
      response.should redirect_to(new_identity_password_path)
    end
    
    it "redirect back to #new if forget password token is invalid" do
      identity.forget_password_token = "abc"
      identity.forget_password_token_created_at = 3.days.ago
      identity.save
      
      get :edit, id: identity.forget_password_token
      
      flash[:reset_password_form][0][:message_type].should == :token_expired
      response.should redirect_to(new_identity_password_path)
    end
  end
  
  describe "put update" do
    it "updates successfully" do
      identity.generate_forget_password_token!
      
      put :update, id: identity.forget_password_token, password: "123", password_confirmation: "1234"
      
      session[:identity_passwords_update__identity].should == assigns(:identity)
      flash[:reset_password_form][0][:message_type].should == :failure
      response.should redirect_to(edit_identity_password_path(id: assigns(:identity).forget_password_token))
    end
    
    it "updates failed" do
      session[:identity_passwords_update__identity] = identity
      
      put :update, id: "don't care", password: "123123", password_confirmation: "123123"
      
      session[:identity_passwords_update__identity].should be_nil
      assigns(:identity).forget_password_token.should be_nil
      assigns(:identity).forget_password_token_created_at.should be_nil
      flash[:default][0][:message_type].should == :success
      response.should redirect_to(root_path)
    end
  end
end
