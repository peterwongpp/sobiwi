require 'spec_helper'

describe BiblesController, focus: true do
  describe "show_chapter" do
    it "assigns correct variables" do
      get :show_chapter, book_code: "gen", chapter: "3"
      assigns(:book).should be(Book.instance_of(controller.chosen_bible.bible_code, "gen"))
      assigns(:chapter).should == 3
    end
  end

  describe "show_verse" do
    it "assigns correct variables" do
      get :show_verse, book_code: "gen", chapter: "2", verse: "4"
      assigns(:verse).should be(Verse.find_by_bible_code_and_book_code_and_chapter_and_verse(controller.chosen_bible.bible_code, "gen", 2, 4))
    end
  end
end
