require 'spec_helper'

describe UsersController, focus: true do
  let(:identity) { Factory.create(:identity) }
  let(:current_user) { Factory.create(:user, uid: identity.id, email: identity.email) }

  before(:each) do
    session[:user_id] = current_user.id
  end
  after(:each) do
    session[:user_id] = nil
  end

  describe "put update" do
    it "updates successfully" do
      put :update, user: { display_name: "new name" }

      assigns(:user).should == current_user
      response.should redirect_to(user_profile_path(current_user))
    end

    it "updates failed" do
      existing_identity = Factory.create(:identity)
      existing_user = Factory.create(:user, uid: existing_identity.id, email: existing_identity.email)

      put :update, user: { email: existing_user.email }

      assigns(:user).should == current_user
      session[:users_update__user].should == current_user
      response.should redirect_to(edit_user_path)
    end
  end

  describe "delete destroy" do
    it "destroy the user" do
      delete :destroy

      assigns(:user).should == current_user
      session[:user_id].should be_nil
      assigns(:user).should_not be_persisted
    end
  end

  describe "get profile" do
    it "show the user's profile" do
      iden = Factory.create(:identity)
      user = Factory.create(:user, uid: iden.id, email: iden.email)

      get :profile, user_id: user.id

      assigns(:user).should == user
      response.should render_template(:profile)
    end
  end

  describe "post follow" do
    it "follow the user" do
      iden = Factory.create(:identity)
      user = Factory.create(:user, uid: iden.id, email: iden.email)

      post :follow, id: user.id

      controller.current_user.should be_following(user)
    end
  end

  describe "GET home" do
    it "show user's homepage" do
      get :home, user_id: current_user.id

      assigns(:user).should == current_user
    end
    it "goes to his own homepage" do
      iden = Factory.create(:identity)
      user = Factory.create(:user, uid: iden.id, email: iden.email)

      get :home, user_id: user.id

      response.should redirect_to(user_home_path(current_user))
    end
  end
end
