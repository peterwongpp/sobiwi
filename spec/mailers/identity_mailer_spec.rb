require "spec_helper"

describe IdentityMailer, focus: true do
  it "should be able to send forget password email" do
    @identity = Factory.create(:identity)
    lambda { IdentityMailer.forget_password(@identity).deliver }.should_not raise_error
  end
end
