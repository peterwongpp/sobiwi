require 'spec_helper'

def login
  @identity = Factory.create(:identity)
  @user = Factory.create(:user, uid: @identity.id, email: @identity.email)

  visit "/login"
  fill_in "auth_key", :with => @user.email
  fill_in "password", :with => "123123"
  click_button I18n.t("words.signin")
  page.should have_content I18n.t("flashes.sessions.create.success")
end

describe "Members", type: :request, focus: true do
  it "can register as new user" do
    visit "/identities/new"
    within("#registration_new_identity") do
      fill_in "name", with: "user1"
      fill_in "email", with: "user1@users.com"
      fill_in "password", with: "123123"
      fill_in "password_confirmation", with: "123123"
      click_button I18n.t("words.register")
    end

    page.should have_content I18n.t("flashes.sessions.create.success")
  end

  it "cannot register without display name" do
    visit "/identities/new"
    within("#registration_new_identity") do
      fill_in "email", with: "user1@users.com"
      fill_in "password", with: "123123"
      fill_in "password_confirmation", with: "123123"
      click_button I18n.t("words.register")
    end

    page.should have_content I18n.t("flashes.users.create.warning")
  end

  it "cannot register without email" do
    visit "/identities/new"
    within("#registration_new_identity") do
      fill_in "name", with: "user1"
      fill_in "password", with: "123123"
      fill_in "password_confirmation", with: "123123"
      click_button I18n.t("words.register")
    end

    page.should have_content I18n.t("flashes.users.create.warning")
  end

  it "cannot register without password" do
    visit "/identities/new"
    within("#registration_new_identity") do
      fill_in "name", with: "user1"
      fill_in "email", with: "user1@users.com"
      fill_in "password_confirmation", with: "123123"
      click_button I18n.t("words.register")
    end

    page.should have_content I18n.t("flashes.users.create.warning")
  end

  it "cannot register without password confirmation" do
    visit "/identities/new"
    within("#registration_new_identity") do
      fill_in "name", with: "user1"
      fill_in "email", with: "user1@users.com"
      fill_in "password", with: "123123"
      click_button I18n.t("words.register")
    end

    page.should have_content I18n.t("flashes.users.create.warning")
  end

  it "can log in" do
    identity = Factory.create(:identity)
    @user = Factory.create(:user, uid: identity.id, email: identity.email)

    visit "/login"
    within("#login_new_identity") do
      fill_in "auth_key", with: @user.email
      fill_in "password", with: "123123"
      click_button I18n.t("words.signin")
    end

    page.should have_content I18n.t("flashes.sessions.create.success")
  end

  it "can reset password if forget password" do
    identity = Factory.create(:identity)
    @user = Factory.create(:user, uid: identity.id, email: identity.email)

    IdentityMailer.should_receive(:forget_password).with(identity).and_return(double('dummy').as_null_object)

    visit "/identity/passwords/new"
    within("#forget_password_new_identity") do
      fill_in "email", with: @user.email
      click_button I18n.t("words.send")
    end

    identity.reload
    @user.reload

    visit "/identity/passwords/#{identity.forget_password_token}/edit"
    within("#forget_password_edit_identity_#{identity.id}") do
      fill_in "password", with: "abcabc"
      fill_in "password_confirmation", with: "abcabc"
      click_button I18n.t("words.save")
    end

    page.should have_content I18n.t("flashes.identity.passwords.update.success")

    identity.reload
    @user.reload

    visit "/login"
    within("#login_new_identity") do
      fill_in "auth_key", with: @user.email
      fill_in "password", with: "abcabc"
      click_button I18n.t("words.signin")
    end

    page.should have_content I18n.t("flashes.sessions.create.success")
  end

  it "should be able to follow other users" do
    login
    @identity2 = Factory.create(:identity)
    @user2 = Factory.create(:user, uid: @identity2.id, email: @identity2.email)

    visit "/users/#{@user2.id}/profile"
    click_link I18n.t("words.follow")

    page.should have_content I18n.t("flashes.users.follow.success", name: @user2.display_name)
  end

  it "should be able to unfollow other users" do
    login
    @identity2 = Factory.create(:identity)
    @user2 = Factory.create(:user, uid: @identity2.id, email: @identity2.email)
    @user.follow!(@user2)

    visit "/users/#{@user2.id}/profile"
    click_link I18n.t("words.unfollow")

    page.should have_content I18n.t("flashes.users.unfollow.success", name: @user2.display_name)
  end

  it "should be able to search user by email after sign in" do
    login
    @identity2 = Factory.create(:identity)
    @user2 = Factory.create(:user, uid: @identity2.id, email: @identity2.email)

    visit "/"
    within("#search-user-form") do
      fill_in "search_user_email", with: @user2.email
      click_button "commit"
    end

    page.should have_content I18n.t("flashes.pages.search_user.success")
  end

  it "should be able to search churches by name" do
    @vc = Factory.create(:virtual_church, name: "This is a long name")

    visit "/"
    within("#search-church-form") do
      fill_in "search_church_name", with: "is"
      click_button "commit"
    end

    page.should have_content @vc.name
  end

  it "should be able to join virtual churches" do
    login
    identity = Factory.create(:identity)
    user = Factory.create(:user, uid: identity.id, email: identity.email)
    @vc = Factory.create(:virtual_church, creator: user)

    visit "/churches/#{@vc.id}"
    click_link I18n.t("words.join")

    page.should have_content I18n.t("flashes.virtual_churches.join.success", name: @vc.name)
  end

  it "should be able to leave virtual churches" do
    login
    identity = Factory.create(:identity)
    user = Factory.create(:user, uid: identity.id, email: identity.email)
    @vc = Factory.create(:virtual_church, creator: user)
    @user.join!(@vc)

    visit "/churches/#{@vc.id}"
    click_link I18n.t("words.leave")

    page.should have_content I18n.t("flashes.virtual_churches.leave.success", name: @vc.name)
  end

  it "should be able to create a virtual church" do
    login

    visit "/"
    click_link I18n.t("words.virtual_churches")
    click_link I18n.t("words.new")

    within("#virtual-church_new_virtual_church") do
      fill_in "virtual-church_virtual_church_name", with: "My church"
      fill_in "virtual-church_virtual_church_description", with: "yo this is my church"
      click_button I18n.t("words.save")
    end

    page.should have_content I18n.t("flashes.users.virtual_churches.create.success")
  end

  it "should be able to edit an owned virtual church" do
    login
    @vc = Factory.create(:virtual_church, creator: @user)

    visit "/"
    click_link I18n.t("words.virtual_churches")

    within("#owned-churches") do
      click_link I18n.t("words.edit")
    end

    within("#virtual-church_edit_virtual_church_#{@vc.id}") do
      fill_in "virtual-church_virtual_church_name", with: "This is a new church name"
      fill_in "virtual-church_virtual_church_description", with: "This is a new church description"
      click_button I18n.t("words.save")
    end

    page.should have_content "This is a new church name"
    page.should have_content "This is a new church description"
  end

  it "should be able to delete an owned virtual church" do
    login
    @vc = Factory.create(:virtual_church, creator: @user)

    visit "/"
    click_link I18n.t("words.virtual_churches")
    page.should have_content @vc.name
    page.should have_content @vc.description

    click_link I18n.t("words.delete")

    visit "/"
    click_link I18n.t("words.virtual_churches")
    page.should_not have_content @vc.name
    page.should_not have_content @vc.description
  end

  it "should be able to view the list of owned virtual churches" do
    login
    @virtual_churches = []
    3.times { @virtual_churches << Factory.create(:virtual_church, creator: @user) }

    visit "/"
    click_link I18n.t("words.virtual_churches")

    @virtual_churches.each do |vc|
      page.should have_content vc.name
    end
  end

  it "should be able to view the list of joined virtual churches" do
    login
    @identity2 = Factory.create(:identity)
    @user2 = Factory.create(:user, uid: @identity2.id, email: @identity2.email)
    @virtual_churches = []
    3.times { |n|
      @virtual_churches << Factory.create(:virtual_church, creator: @user2)
      Factory.create(:membership, user: @user, virtual_church: @virtual_churches[n])
    }

    visit "/"
    click_link I18n.t("words.virtual_churches")

    @virtual_churches.each do |vc|
      page.should have_content vc.name
    end
  end
end