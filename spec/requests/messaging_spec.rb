require 'spec_helper'

def login
  @identity = Factory.create(:identity)
  @user = Factory.create(:user, uid: @identity.id, email: @identity.email)

  visit "/login"
  fill_in "auth_key", :with => @user.email
  fill_in "password", :with => "123123"
  click_button I18n.t("words.signin")
  page.should have_content I18n.t("flashes.sessions.create.success")
end

describe "Messaging", type: :request, focus: true do
  it "should have a button on topbar to link to the messaging center" do
    login
    @message_thread = Factory.create(:message_thread)
    @message_thread_user = Factory.create(:message_thread_user, message_thread: @message_thread, user: @user)
    @message = Factory.create(:message, message_thread: @message_thread, user: @user)

    visit "/"
    within(".navbar") do
      page.should have_selector("a[href='/message_threads']")
      click_link I18n.t("words.messages")
    end

    page.should have_content(@message.content)
  end

  it "should show a message thread with paginated messages" do
    login
    @message_thread = Factory.create(:message_thread)
    @message_thread_user = Factory.create(:message_thread_user, message_thread: @message_thread, user: @user)
    @messages = []
    11.times { @messages << Factory.create(:message, message_thread: @message_thread, user: @user) }

    visit "/message_threads/#{@message_thread.id}"

    10.times do |n|
      page.should have_content(@messages[n].content)
    end
    page.should_not have_content(@messages.last.content)

    click_link "2"

    # to avoid the accidental match of Mesasge 1 and Message 11
    9.times do |n|
      page.should_not have_content(@messages[n+1].content)
    end
    page.should have_content(@messages.last.content)
  end

  it "should have a form inside a thread to post message" do
    login
    @message_thread = Factory.create(:message_thread)
    @identity2 = Factory.create(:identity)
    @user2 = Factory.create(:user, uid: @identity2.id, email: @identity2.email)
    Factory.create(:message_thread_user, message_thread: @message_thread, user: @user)
    Factory.create(:message_thread_user, message_thread: @message_thread, user: @user2)

    visit "/message_threads/#{@message_thread.id}"

    page.should have_selector("form[class~='new_message']")

    within("form[class~='new_message']") do
      fill_in "message_content", with: "Yo I am a message :)"
      click_button I18n.t("words.send")
    end

    visit "/message_threads/#{@message_thread.id}"
    page.should have_content "Yo I am a message :)"
  end

  it "should be able to delete messages post by myself" do
    login
    @message_thread = Factory.create(:message_thread)
    Factory.create(:message_thread_user, message_thread: @message_thread, user: @user)
    @message = Factory.create(:message, message_thread: @message_thread, user: @user)

    visit "/message_threads/#{@message_thread.id}"
    page.should have_content(@message.content)

    click_link "delete_message_#{@message.id}"

    visit "/message_threads/#{@message_thread.id}"
    page.should_not have_content(@message.content)
  end

  it "should be able to create thread to talk with as many friends as he want" do
    login
    @users = []
    10.times {
      identity = Factory.create(:identity)
      @users << Factory.create(:user, uid: identity.id, email: identity.email)
    }
    10.times { |n| @users[n].follow!(@user) }

    visit "/message_threads"
    click_link I18n.t("words.new")

    within "form.new_message" do
      check "message_thread_user_ids_#{@users[0].id}"
      check "message_thread_user_ids_#{@users[3].id}"
      check "message_thread_user_ids_#{@users[4].id}"
      check "message_thread_user_ids_#{@users[5].id}"
      check "message_thread_user_ids_#{@users[8].id}"
      check "message_thread_user_ids_#{@users[9].id}"
      fill_in Message.human_attribute_name(:content), with: "this is my first message :)"
      click_button I18n.t("words.save")
    end

    page.should have_content("this is my first message :)")
  end

  it "should reuse the existing thread" do
    login
    @identity2 = Factory.create(:identity)
    @user2 = Factory.create(:user, uid: @identity2.id, email: @identity2.email)
    @user.follow!(@user2)

    @mt = Factory.build(:message_thread)
    @mt.message_thread_users.build(user: @user)
    @mt.message_thread_users.build(user: @user2)
    @mt.save!

    original_count = MessageThread.count

    visit "/message_threads"
    click_link I18n.t("words.new")

    within "form.new_message" do
      check "message_thread_user_ids_#{@user2.id}"
      fill_in Message.human_attribute_name(:content), with: "yo me again"
      click_button I18n.t("words.save")
    end

    page.should have_content("yo me again")
    MessageThread.count.should == original_count
  end
end