Sobiwi::Application.routes.draw do
  # The priority is based upon order of creation:
  # first created -> highest priority.

  root to: 'pages#index'

  resources :message_threads do
    resources :messages
  end

  resource :user, except: [:new, :create, :show] do
    resources :virtual_churches, path: "churches", controller: "users/virtual_churches"
  end
  resources :users, only: [] do
    get 'home'
    get 'profile'
    member do
      post 'follow'
    end
  end

  resources :notes, only: [:new, :create, :destroy]

  resources :virtual_churches, except: [:index], path: "churches" do
    member do
      post 'join'
    end
  end

  get '/login' => 'sessions#new', as: :login
  match '/auth/:provider/callback', to: 'sessions#create', as: :login_handle
  match '/auth/failure', to: 'sessions#failure'
  match '/logout', to: 'sessions#destroy', as: :logout

  resources :identities, only: [:new]
  namespace :identity do
    resources :passwords, only: [:new, :create, :edit, :update]
  end

  get '/about' => redirect("http://i.cs.hku.hk/fyp/2011/fyp11011/public_html")

  scope '/read' do
    get '/:book_code/:chapter' => 'bibles#show_chapter', as: :chapter
    get '/:book_code/:chapter/:verse' => 'bibles#show_verse', as: :verse
  end

  get '/map' => 'pages#map'
  get '/search-user' => 'pages#search_user', as: :search_user
  get '/search-church' => 'pages#search_church', as: :search_church

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with 'root'
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with 'rake routes'

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
