DEFAULT_LOCALE = :"zh-HK"
DEFAULT_BIBLE = :unv

LOCALES = %w(en zh-HK)

BIBLES = %w(kjv unv)
BOOKS = {
  old: %w(gen ex lev num deut josh judg ruth 1sam 2sam 1kin 2kin 1chr 2chr ezra neh esth job ps prov eccl song is jer lam ezek dan hos joel amos obad jon mic nah zeph hab hag zech mal),
  new: %w(matt mark luke john acts rom 1cor 2cor gal eph phil col 1thess 2thess 1tim 2tim titus philem heb james 1pet 2pet 1john 2john 3john jude rev)
}