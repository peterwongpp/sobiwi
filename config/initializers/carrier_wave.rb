if Rails.env.production?
  CarrierWave.configure do |config|
    config.fog_credentials = {
      :provider               => 'AWS',
      :aws_access_key_id      => ENV['AWS_S3_ACCESS'],
      :aws_secret_access_key  => ENV['AWS_S3_SECRET'],
      :region                 => 'us-east-1'
    }
    config.fog_directory  = 'sobiwi'
    config.fog_public     = true
    config.fog_attributes = {'Cache-Control' => 'max-age=315576000'}
  end
end
