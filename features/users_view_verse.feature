@bible
Feature: User view verse
  In order to read a verse of bible
  As a user
  I could press a link to the verse on the chapter page
  
  Background:
    Given the following verses exist:
      | bible_code  | book_code | chapter | verse | content |
      | kjv         | gen       | 1       | 1     | In the beginning God created the heaven and the earth. |
      | kjv         | gen       | 1       | 2     | And the earth was without form, and void; and darkness was upon the face of the deep. And the Spirit of God moved upon the face of the waters. |
      | kjv         | gen       | 1       | 3     | And God said, Let there be light: and there was light. |
  
  Scenario Outline: press the link and read the verse
    Given I am on the chapter page of gen 1
    
    When I click verse name of <bible_code> <book_code> <chapter>:<verse>
    
    Then I should see <verse_content>
    
  Examples:
    | bible_code  | book_code | chapter | verse | verse_content |
    | kjv         | gen       | 1       | 1     | In the beginning God created the heaven and the earth. |
    | kjv         | gen       | 1       | 2     | And the earth was without form, and void; and darkness was upon the face of the deep. And the Spirit of God moved upon the face of the waters. |
    | kjv         | gen       | 1       | 3     | And God said, Let there be light: and there was light. |