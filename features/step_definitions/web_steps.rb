Given /^I am on (.+)$/ do |page_name|
  visit path_to(page_name)
end

When /^I fill in "([^"]*)" with "([^"]*)"$/ do |field_text_or_id_or_title, value|
  fill_in field_text_or_id_or_title, :with => value
end

When /^I fill in the following:$/ do |fields|
    select_tag    = /^(.+\S+)\s*(?:\(select\))$/
    check_box_tag = /^(.+\S+)\s*(?:\(checkbox\))$/
    radio_button  = /^(.+\S+)\s*(?:\(radio\))$/
    file_field    = /^(.+\S+)\s*(?:\(file\))$/
  
    fields.rows_hash.each do |name, value|
      case name
      when select_tag
        select value, :from => $1
      when check_box_tag
        case value
        when 'check'
          check($1)
        when 'uncheck'
          uncheck($1)
        else
          raise 'checkbox values: check|uncheck!'
        end
      when radio_button
        step %{I choose "#{$1}"}
      when file_field
        step %{I attach the file "#{value}" to "#{$1}"}
      else
        step %{I fill in "#{name}" with "#{value}"}
      end
    end
end

When /^I click (.+)$/ do |link_title_id_text|
  link_title_id_text = case link_title_id_text
  when /^verse name of ([^ ]+) ([^ ]+) (\d+):(\d+)$/
    verse = Verse.find_by_bible_code_and_book_code_and_chapter_and_verse($1, $2, $3, $4)
    verse.name
  else
    link_title_id_text
  end
  
  click_link(link_title_id_text)
end

When /^I press (.+)$/ do |button|
  click_button button
end

Then /^I should see (.*)$/ do |message|
  page.should have_content(message)
end

Then /^I should see "(.+)" (?:button|link)$/ do |button_name| 
  page.has_link?(button_name).should be_true
end

Then /^I should not see "(.+)" (?:button|link)$/ do |button_name| 
  page.has_no_link?(button_name).should be_true
end

Then /^I should see link to "(.+)"$/ do |url| 
  page.has_xpath?("//a[@href='#{url}']").should be_true
  #page.has_xpath?("//a", :href => url).should be_true
end 
Then /^I should not see link to "(.+)"$/ do |url| 
  page.has_xpath?("//a[@href='#{url}']").should be_false
  #page.has_xpath?("//a", :href => url).should be_false
end 

Then /^I should be redirected to (.+)$/ do |page|
  current_path.should == path_to(page)
end

module NavigationHelpers
  def path_to(page_name)
    case page_name
    when /^the home page$/
      root_path
    
    when /^the chapter page of ([^ ]+) (\d+)$/
      chapter_path({
        book_code: $1,
        chapter: $2
      })
    
    when /^the verse page of ([^ ]+) (\d+):(\d+)$/
      verse_path({
        book_code: $1,
        chapter: $2,
        verse: $3
      })
    
    when /^the (.*) page$/
      path_components = $1.split(/\s+/)
      self.send(path_components.push('path').join('_').to_sym)
    else
      page_name
    end
  end
end

World(NavigationHelpers)
